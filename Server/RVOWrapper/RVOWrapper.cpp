#include "stdafx.h"
#include <map>
#include "../RVO/RVO.h"

// 存放所有模拟器对象
std::map<int, RVO::RVOSimulator*> ss;

namespace RVO
{
	// 将 RVO 接口包装给 c# 使用
	public ref class RVOWrapper
	{
	private:
		static int idSeq = 0;
		static int GetNextID() { return ++idSeq; }

	public:

		// 最大保有 RVO 模拟器的数量
		static int SimulatorMaxNum = 64;

		// 创建一个 RVO 模拟器，并返回 id，不使用了需要销毁
		static int CreateRVOSimulator()
		{
			if (ss.size() >= SimulatorMaxNum)
				throw "reach the SimulatorMaxNum limitation";

			int sid = GetNextID();
			RVOSimulator* s = new RVOSimulator();
			ss[sid] = s;
			return sid;
		}

		// 销毁一个 RVO 模拟器
		static void DestroyRVOSimulator(int sid)
		{
			RVOSimulator* s = ss[sid];
			ss.erase(sid);
			delete s;
		}

		// 设置模拟器的全局时间步长
		static void SetTimeStep(int sid, float timeStep)
		{
			RVOSimulator* s = ss[sid];
			s->setTimeStep(timeStep);
		}

		// 设置模拟器的默认参数
		static void SetAgentDefaults(int sid, float neighborDist, int maxNeighbors, float timeHorizon, float timeHorizonObst, float radius, float maxSpeed, float vx, float vy)
		{
			RVOSimulator* s = ss[sid];
			s->setAgentDefaults(neighborDist, maxNeighbors, timeHorizon, timeHorizonObst, radius, maxSpeed, Vector2(vx, vy));
		}

		// 添加碰撞单元
		static int AddAgent(int sid, float px, float py, float radius, float maxSpeed)
		{
			int aid = GetNextID();
			RVOSimulator* s = ss[sid];
			s->addAgent(aid, Vector2(px, py), radius, maxSpeed);
			return aid;
		}

		// 移除碰撞单元
		static void RemoveAgent(int sid, int aid)
		{
			RVOSimulator* s = ss[sid];
			s->removeAgent(aid);
		}

		// 刷新参数
		static void RefreshRVOAgent(int sid, int aid, float x, float y, float radius, float maxSpeed)
		{
			RVOSimulator* s = ss[sid];
			s->RefreshRVOAgent(aid, Vector2(x, y), radius, maxSpeed);
		}

		// 添加固定障碍
		static unsigned long long addObstacle(int sid, float vsArr[], int vsCount)
		{
			RVOSimulator* s = ss[sid];
			std::vector<Vector2> vss;

			for (int i = 0; i < vsCount; i++)
				vss.push_back(Vector2(vsArr[i * 2], vsArr[i * 2 + 1]));

			return s->addObstacle(vss);
		}

		// 构建障碍
		static void ProcessObstacles(int sid)
		{
			RVOSimulator* s = ss[sid];
			s->processObstacles();
		}

		// 执行一步
		static void DoStep(int sid)
		{
			RVOSimulator* s = ss[sid];
			s->doStep();
		}

		// 获取模拟器全局时间
		static float GetGlobalTime(int sid)
		{
			RVOSimulator* s = ss[sid];
			return s->getGlobalTime();
		}

		// 迭代所有对象的位置信息
		static System::Collections::ArrayList^ GetAllAgentPosAndVelocity(int sid)
		{
			System::Collections::ArrayList^ data = gcnew System::Collections::ArrayList();
			RVOSimulator* s = ss[sid];
			for (size_t i = 0; i < s->getNumAgents(); ++i)
			{
				int id = (int)s->getAgentID(i);
				data->Add(id);

				Vector2 p = s->getAgentPosition(i);
				data->Add(p.x());
				data->Add(p.y());

				Vector2 v = s->getAgentVelocity(i);
				data->Add(v.x());
				data->Add(v.y());
			}

			return data;
		}

		// 设置对象的期望速度
		static void SetAgentPrefVelocity(int sid, System::Collections::Generic::Dictionary<int, System::Collections::Generic::KeyValuePair<float, float>>^ vArr)
		{
			RVOSimulator* s = ss[sid];
			for (size_t i = 0; i < s->getNumAgents(); ++i)
			{
				int id = (int)s->getAgentID(i);
				if (!vArr->ContainsKey(id))
					continue;

				System::Collections::Generic::KeyValuePair<float, float> v = vArr[id];
				float vx = v.Key;
				float vy = v.Value;
				s->setAgentPrefVelocity(i, Vector2(vx, vy));
			}
		}
	};
}
