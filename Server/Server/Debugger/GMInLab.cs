﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Swift;
using SCM;

namespace Server
{
    public class GMInLab : NetComponent
    {
        ConsoleInput ci;
        public override void Init()
        {
            ci = GetCom<ConsoleInput>();

            ci.OnCommand("switchplayer", (ps) =>
            {
                var cnt = 0;
                var brm = GetCom<BattleRoomManager>();

                foreach (Room4Server r in brm.AllRooms)
                {
                    var usr1 = r.UsrsID[1];
                    var usr2 = r.UsrsID[2];
                    r.UsrsID[1] = usr2;
                    r.UsrsID[2] = usr1;

                    var p1 = r.Players[1];
                    var p2 = r.Players[2];
                    r.Players[1] = p2;
                    r.Players[2] = p1;

                    cnt++;

                    r.Broadcast("PlayerSwitched", null);
                }

                return "switched: " + cnt;
            });

            ci.OnCommand("+coins", (ps) =>
            {
                if (ps == null || ps.Length == 0)
                    return "need user id";

                var uid = ps[0];
                var num = int.Parse(ps[1]);
                var sc = GetCom<SessionContainer>();
                var s = sc[uid];
                if (s == null)
                    return "can not find online user: " + uid;

                s.Usr.AddCoins(num);
                s.Conn.Send2Usr("AddCoins", (buff) => { buff.Write(num); });
                return "add " + num + " coins to " + uid;
            });

            ci.OnCommand("win", (ps) =>
            {
                if (ps == null || ps.Length == 0)
                    return "need user id";

                var uid = ps[0];
                var brm = GetCom<BattleRoomManager>();

                foreach (Room4Server r in brm.AllRooms)
                {
                    if (r.UsrsID[1] == uid || r.UsrsID[2] == uid)
                    {
                        r.BattleEnd(uid);
                        return "battle ended with winner: " + uid;
                    }
                }

                return "find no Battle with userid: " + uid;
            });

            ci.OnCommand("lose", (ps) =>
            {
                if (ps == null || ps.Length == 0)
                    return "need user id";

                var uid = ps[0];
                var brm = GetCom<BattleRoomManager>();

                foreach (Room4Server r in brm.AllRooms)
                {
                    if (r.UsrsID[1] == uid || r.UsrsID[2] == uid)
                    {
                        var winner = r.UsrsID[1] == uid ? r.UsrsID[2] : r.UsrsID[1];
                        r.BattleEnd(winner);
                        return "battle ended with winner: " + winner;
                    }
                }

                return "find no Battle with userid: " + uid;
            });

            ci.OnCommand("openall4", (ps) =>
            {
                if (ps == null || ps.Length == 0)
                    return "need user id";

                var uid = ps[0];
                return OpenAll(uid, true);
            });

            ci.OnCommand("add10cards", (ps) =>
            {
                if (ps == null || ps.Length < 2)
                    return "need user id";

                var uid = ps[0];
                var c = ps[1];
                var sc = GetCom<SessionContainer>();
                var s = sc[uid];
                if (s == null)
                    return "can not find online user: " + uid;
                else if (UnitConfiguration.AllConfigNames.FirstIndexOf(c) < 0)
                    return "no such card: " + c;

                var num = 10;
                var usrInfo = s.Usr.Info;
                var uLst = new List<string>();
                usrInfo.AddCard(c, num);
                uLst.Add(c);

                s.Usr.Update();
                s.Conn.Send2Usr("AddCards", (buff) => { buff.Write(c); buff.Write(num); });
                return "open all unit for " + uid;
            });

            GetCom<LoginManager>().BeforeUserLogin += (Session s, bool isNew) =>
            {
                ////---- 这里加个 trick，方便内部测试
                var usrInfo = s.Usr.Info;
                usrInfo.AddPassedPVELevel("Training00");
                usrInfo.AddPassedPVELevel("Training01");
                usrInfo.AddPassedPVELevel("Training02");
                OpenAll(s.ID, false);
            };
        }

        string OpenAll(string uid, bool sendMsg)
        {
            var sc = GetCom<SessionContainer>();
            var s = sc[uid];
            if (s == null)
                return "can not find online user: " + uid;

            var usrInfo = s.Usr.Info;
            var uLst = new List<string>();
            foreach (var c in UnitConfiguration.AllConfigNames)
            {
                var cfg = UnitConfiguration.GetDefaultConfig(c);
                if (cfg.NeutralType || usrInfo.GetCardLv(c) > 0)
                    continue;

                usrInfo.AddUnit(c, 1);
                uLst.Add(c);
            }

            s.Usr.Update();
            if (sendMsg)
                s.Conn.Send2Usr("AddUnits", (buff) => { buff.Write(uLst.ToArray()); });
            return "all units opened for: " + uid;
        }
    }
}
