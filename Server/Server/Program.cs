﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swift;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var port = 9528;
            var srvAddr = "127.0.0.1";
            var noConsole = false;
            var onlyConsoleAgent = false;
            foreach (var p in args)
            {
                if (p == "-noconsole")
                    noConsole = true;
                else if (p.StartsWith("-p"))
                    port = int.Parse(p.Substring(2));
                else if (p == "-console-agent")
                    onlyConsoleAgent = true;
                else if (p.StartsWith("-h"))
                    srvAddr = p.Substring(2);
                else
                    Console.WriteLine("unknown parameter: " + p);
            }

            var srv = new GameServer();

            if (!onlyConsoleAgent)
            {
                var startSrv = ServerBuilder.BuildLibServer(srv, port);
                if (!noConsole)
                    srv.Get<ConsoleInput>().Start(); // 启用控制台输入

                startSrv();
            }
            else
            {
                ServerBuilder.BuildConsoleAgent(srv, srvAddr, port)();
            }
        }
    }
}
