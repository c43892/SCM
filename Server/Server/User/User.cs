﻿using System;
using System.Collections.Generic;
using Swift;
using SCM;

namespace Server
{
    /// <summary>
    /// 用户对象
    /// </summary>
    public class User : DataItem<string>
    {
        // 用户密码
        public string Pwd;

        // 用户信息
        public UserInfo Info = new UserInfo();

        protected override void Sync()
        {
            BeginSync();
            SyncString(ref ID);
            SyncString(ref Pwd);
            SyncObj(ref Info);
            EndSync();
        }

        public StableDictionary<string, KeyValuePair<int, int>> Cards { get { return Info.Cards; } }

        // 直接添加单位
        public void AddUnit(string unitType, int lv = 1)
        {
            Info.AddUnit(unitType, lv);
            Update();
        }

        // 添加卡片
        public void AddCard(string unitType, int n)
        {
            Info.AddCard(unitType, n);
            Update();
        }

        // 卡片升级，返回新的等级
        public bool CombineOrUpgradeUnit(string unitType)
        {
            var b = Info.CombineOrUpgradeUnit(unitType);
            Update();
            return b;
        }

        // 增加金币
        public void AddCoins(int num)
        {
            Info.Coins += num;
            Update();
        }
    }
}
