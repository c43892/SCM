﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Swift;
using Swift.Math;
using SCM;

namespace Server
{
    public class AIPVE002 : AIComputerOpponent
    {
        public AIPVE002(string name, Room4Server room, int player)
            : base(name, room, player)
        {
        }

        public override void Init()
        {
            sm.NewState("waiting4PlayerBunker").Run(null).AsDefault();
            sm.NewState("attacking").Run((st, te) =>
            {
                // 产兵
                SrvRoom.SrvAddUnitAt(1, "Firebat", 2, SrvRoom.MapSize / 2 + new Vec2(0, -200));

                // 冲向玩家基地
                var ccs = SrvRoom.GetUnitsByType("CommanderCenter", 2);
                if (ccs.Length == 0)
                    return;

                var sds = SrvRoom.GetUnitsByType("Firebat", 1);
                foreach (var sd in sds)
                    SrvRoom.SrvSetPath(sd, new Vec2[] { ccs[0].Pos }, false);
            });
            sm.NewState("waiting4Attacking").Run(null);

            // 等玩家的炮台建造好
            sm.Trans().From("waiting4PlayerBunker").To("attacking").When((st) =>
            {
                var bcks = SrvRoom.GetUnitsByType("Bunker", 2);
                foreach (var bck in bcks)
                {
                    if (!bck.BuildingCompleted)
                        return false;
                }

                return true;
            });

            // 一轮一轮进攻
            sm.Trans().From("attacking").To("waiting4Attacking").When((st) =>
            {
                return SrvRoom.GetUnitsByType("Firebat", 1).Length > 0;
            });

            sm.Trans().From("waiting4Attacking").To("attacking").When((st) =>
            {
                return SrvRoom.GetUnitsByType("Firebat", 1).Length == 0;
            });
        }
    }
}
