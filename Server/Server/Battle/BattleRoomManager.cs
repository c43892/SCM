﻿using System;
using System.Collections.Generic;
using Swift;
using SCM;
using Swift.Math;

namespace Server
{
    /// <summary>
    /// 战斗房间管理器
    /// </summary>
    public class BattleRoomManager : Component, IFrameDrived
    {
        UserPort UP;
        SessionContainer SC;

        // 所有活动的战斗房间
        List<Room4Server> rooms = new List<Room4Server>();
        public Room4Server[] AllRooms { get { return rooms.ToArray(); } }

        // 记录用户所在房间
        StableDictionary<string, Room4Server> usr2room = new StableDictionary<string, Room4Server>();

        public override void Init()
        {
            SC = GetCom<SessionContainer>();
            var lgMgr = GetCom<LoginManager>();
            lgMgr.OnUserDisconnecting += OnUserDisconnected;
            UP = GetCom<UserPort>();

            RedirectRoomMessage("SetPath");
            RedirectRoomMessage("ConstructBuilding");
            RedirectRoomMessage("CancelBuilding");
            RedirectRoomMessage("BuildingLevelUp");
            RedirectRoomMessage("ConstructBattleUnit");
            RedirectRoomMessage("ConstructAccessoryUnit");
            RedirectRoomMessage("SetSpecificAttackTarget");
            RedirectRoomMessage("DropSoldierFromCarrier");
            RedirectRoomMessage("ReleaseBattleUnits");
            UP.OnMessage("Surrender", OnSurrender);
            UP.OnRequest("GetReplayList", OnGetReplayList);
            UP.OnRequest("GetReplay", OnGetReplay);

            Room4Server.LoadAllPVPReplays();
        }

        // 创建 PVE 战斗房间
        public Room4Server CreatePVERoom(string usr, string lvID)
        {
            var s = SC[usr];

            // 创建新房间
            var roomID = usr + "_vs_AIRobot";

            // 创建 ai
            var aiID = roomID + "_" + lvID;
            var r = new Room4Server(roomID, aiID, s, new Vec2(600, 1500), lvID);
            var ai = r.CreateComputerAI(r.Lv.LevelID, r.GetNoByUser(aiID));
            ai.FindPath = (radius, src, dst, cb) => { RequestPathFromClient(s, radius, src, dst, cb); };

            rooms.Add(r);
            usr2room[usr] = r;
            return r;
        }

        // 从客户端请求寻路数据
        void RequestPathFromClient(Session s, Fix64 r, Vec2 src, Vec2 dst, Action<Vec2[]> cb)
        {
            var conn = s.Conn;
            if (conn == null)
                return;

            var buff = conn.BeginRequest("ServerPort", (data) =>
            {
                var path = data.ReadVec2Arr();
                if (path == null || path.Length == 0)
                    path = new Vec2[] { dst };

                cb(path);
            }, null);

            buff.Write("FindPath");
            buff.Write(src);
            buff.Write(dst);
            buff.Write(r);
            conn.End(buff);
        }

        // 创建 PVP 战斗房间
        public Room4Server CreatePVPRoom(string usr1, string usr2)
        {
            var s1 = SC[usr1];
            var s2 = SC[usr2];

            if (s1 == null || s2 == null)
                return null;

            // 创建新房间
            var roomID = usr1 + "_vs_" + usr2;
            var r = new Room4Server(roomID, s1, s2, new Vec2(600, 1500), "PVP");
            rooms.Add(r);
            usr2room[usr1] = r;
            usr2room[usr2] = r;
            return r;
        }

        // 转递房间消息
        void RedirectRoomMessage(string op)
        {
            UP.OnMessage(op, (Session s, IReadableBuffer data) =>
            {
                var usr = s.ID;
                var r = usr2room.ContainsKey(usr) ? usr2room[usr] : null;
                if (r == null)
                    return;

                r.OnMessage(op, usr, data);
            });
        }

        // 推动游戏进度
        public void OnTimeElapsed(int te)
        {
            // 推动所有房间的游戏逻辑, 并记录已经结束的房间
            var roomsFinished = new List<Room4Server>();
            foreach (var r in rooms)
            {
                if (!r.Finished)
                {
                    var winner = r.CheckWinner();
                        if (winner >= 0) // 0 是平局
                            r.BattleEnd(r.UsrsID[winner]);
                }

                if (r.Finished)
                    roomsFinished.Add(r);
                else
                    r.OnTimeElapsed(te);
            }

            // 丢弃已结束的房间
            foreach (var r in roomsFinished)
            {
                foreach (var usr in r.UsrsID)
                {
                    if (usr != null)
                        usr2room.Remove(usr);

                    rooms.Remove(r);
                }
            }
        }

        // 获取玩家当前房间
        public Room4Server GetUserRoom(string usr)
        {
            return usr2room.ContainsKey(usr) ? usr2room[usr] : null;
        }

        // 玩家退出或掉线
        void OnUserDisconnected(Session s)
        {
            OnSurrender(s, null);
        }

        // 玩家投降
        void OnSurrender(Session s, IReadableBuffer data)
        {
            if (!usr2room.ContainsKey(s.ID))
                return;

            var r = usr2room[s.ID];
            if (r == null)
                return;

            var loser = r.GetNoByUser(s.ID);
            var winner = loser == 1 ? 2 : 1;
            r.BattleEnd(r.UsrsID[winner]);
            r.RemoveSession(s);
        }

        // 调取最近录像列表
        void OnGetReplayList(Session s, IReadableBuffer data, IWriteableBuffer buff)
        {
            var maxNum = data.ReadInt();
            var arr = Room4Server.AllReplayTitles;
            var lst = new List<string>();
            for (var i = 0; i < maxNum && i < arr.Length; i++)
            {
                var n = arr.Length - 1 - i;
                lst.Add(arr[n]);
            }

            buff.Write(lst.ToArray());
        }

        // 调取录像
        void OnGetReplay(Session s, IReadableBuffer data, IWriteableBuffer buff)
        {
            var replayName = data.ReadString();
            var r = Room4Server.GetReplay(replayName);
            buff.Write(r != null);
            if (r != null)
                ReplayUtils.WriteReplay(r, buff);
        }
    }
}
