﻿Shader "SCM/MainCameraCombineAll"
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "white" {}
		_VisionTex ("Vision Texture", 2D) = "white" {}
		_BuildingHistoryTex("Building Texture", 2D) = "white" {}
		_IndicatorTex("Indicator Texture", 2D) = "white" {}
		_TurnOnBattleFog("FogSwitch", int) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _VisionTex;
			sampler2D _BuildingHistoryTex;
			sampler2D _IndicatorTex;
			int _TurnOnBattleFog;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 main = tex2D(_MainTex, i.uv);
				fixed4 vision = tex2D(_VisionTex, i.uv);
				fixed4 history = tex2D(_BuildingHistoryTex, i.uv);
				fixed4 bb = tex2D(_IndicatorTex, i.uv);

				if (_TurnOnBattleFog > 0)
				{
					fixed a = (vision.r + vision.g + vision.b) / 3;
					fixed alphaThreshold = 0.25;
					if (a <= 0)
						return history * alphaThreshold;
					else
					{
						fixed4 c = bb * bb.a + main * (1 - bb.a);

						if (a < alphaThreshold)
							c = c * a + history * (alphaThreshold - a);
						else
							c = c * a;

						return c;
					}
				}
				else
				{
					fixed4 c = bb * bb.a + main * (1 - bb.a);
					return c;
				}
			}
			ENDCG
		}
	}
}
