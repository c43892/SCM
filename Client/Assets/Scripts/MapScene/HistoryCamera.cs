﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoryCamera : MonoBehaviour {

    public RenderTexture VisionTex;
    public RenderTexture GroundTex;
    public RenderTexture BuildingTex;
    public Material CameraMat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        CameraMat.SetTexture("_VisionTex", VisionTex);
        CameraMat.SetTexture("_GroundTex", GroundTex);
        CameraMat.SetTexture("_BuildingTex", BuildingTex);
        Graphics.Blit(source, destination, CameraMat, 0);
    }
}
