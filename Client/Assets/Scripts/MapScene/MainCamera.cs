﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SCM;
using Swift.Math;

public class MainCamera : SCMBehaviour {

    public int Oblique = 55;

    RenderTexture VisionTex;
    RenderTexture BuildingTex;
    RenderTexture BuildingHistoryTex;
    RenderTexture UIIndicatorTex;

    public Light MainLight;
    public Camera VisionCamera;
    public Camera GroundCamera;
    public Camera BuildingCamera;
    public Camera UIIndicatorCamera;
    public Camera BuildingHistoryCamera;

    public Material CameraMat;
    public Canvas Canvas;

    public RenderTexture GetVisionTex()
    {
        return VisionTex;
    }

    // 开启战争迷雾
    public bool TurnOnBattleFog = true;

    // 记录下灯光初始角度，为 player1 设置
    Quaternion MainLightInitRot = Quaternion.identity;

    protected override void StartOnlyOneTime()
    {
        var rt = Canvas.GetComponent<RectTransform>();
        var sw = (int)rt.rect.width;
        var sh = (int)rt.rect.height;

        VisionTex = new RenderTexture(sw, sh, 0, RenderTextureFormat.ARGB32);
        BuildingTex = new RenderTexture(sw, sh, 16, RenderTextureFormat.ARGB32);
        BuildingHistoryTex = new RenderTexture(sw, sh, 16, RenderTextureFormat.ARGB32);
        UIIndicatorTex = new RenderTexture(sw, sh, 16, RenderTextureFormat.ARGB32);

        VisionCamera.targetTexture = VisionTex;
        BuildingCamera.targetTexture = BuildingTex;
        UIIndicatorCamera.targetTexture = UIIndicatorTex;

        var hc = BuildingHistoryCamera.GetComponent<HistoryCamera>();
        hc.VisionTex = VisionTex;
        hc.BuildingTex = BuildingTex;
        BuildingHistoryCamera.targetTexture = BuildingHistoryTex;

        MainLightInitRot = MainLight.transform.rotation;

        Room4Client.OnBeforeBattleBegin += OnBeforeBattleBegin;
        Room4Client.OnBattleEnd += OnBattleEnd;
    }

    private void OnBeforeBattleBegin(string usr1, string usr2, Vec2 mapSize, bool inReplay)
    {
        AdjustCamera(mapSize);
        StartCoroutine(Prepare());

        // player2 需要调一下灯光角度
        MainLight.transform.rotation = GameCore.Instance.MePlayer == 2 ?
            Quaternion.AngleAxis(180, Vector3.up) * MainLightInitRot :
            MainLightInitRot;
    }

    public void Clear()
    {
        BuildingHistoryCamera.gameObject.SetActive(false);
        RenderTexture rt = UnityEngine.RenderTexture.active;
        UnityEngine.RenderTexture.active = BuildingHistoryTex;
        GL.Clear(true, true, Color.clear);
        UnityEngine.RenderTexture.active = rt;
    }

    private void OnBattleEnd(Room r, string winner, bool inReplay)
    {
        Clear();
    }

    void AdjustCamera(Vec2 mapSize)
    {
        var me = GameCore.Instance.MePlayer;
        transform.localRotation = Quaternion.Euler(90 - Oblique, me == 2 ? 180 : 0, 0);

        var rt = Canvas.GetComponent<RectTransform>();
        var sw = (int)rt.rect.width;
        var sh = (int)rt.rect.height;
        var a = Oblique * Mathf.PI / 180;
        // var mw = sw;
        var mh = sh / Mathf.Cos(a);
        var offsetX = (float)mapSize.x / 2;
        var offsetY = mh * Mathf.Sin(a) / 2 * Mathf.Cos(a);
        var offsetZ = (float)mapSize.y / 2 + mh * Mathf.Sin(a) / 2 * Mathf.Sin(a) * (me == 2 ? 1 : -1);
        var camPos = new Vector3(offsetX, offsetY, offsetZ);
        var upOffset = transform.localRotation * new Vector3(0, 0, -100);
        var uiTitleOffset = transform.localRotation * new Vector3(0, 30, 0);
        transform.localPosition = camPos + upOffset + uiTitleOffset;
    }

    IEnumerator Prepare()
    {
        yield return new WaitForEndOfFrame();
        GroundCamera.gameObject.SetActive(true);
        GroundCamera.targetTexture = BuildingHistoryTex;
        GroundCamera.Render();
        GroundCamera.gameObject.SetActive(false);
        BuildingHistoryCamera.gameObject.SetActive(true);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        BuildingHistoryCamera.Render();

        CameraMat.SetInt("_TurnOnBattleFog", TurnOnBattleFog ? 1 : 0);
        CameraMat.SetTexture("_VisionTex", VisionTex);
        CameraMat.SetTexture("_BuildingHistoryTex", BuildingHistoryTex);
        CameraMat.SetTexture("_IndicatorTex", UIIndicatorTex);
        Graphics.Blit(source, destination, CameraMat, 0);
    }
}
