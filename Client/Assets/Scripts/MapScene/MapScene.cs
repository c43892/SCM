﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Swift;
using SCM;
using Swift.Math;
using UnityEngine.AI;
using System.Collections;

public class MapScene : SCMBehaviour {

    public UnitCreator UC;
    public EffectCreator EC;
    public Transform Units;
    public MapGround MG;
    public IndicatorUILayer IndLayer;

    Room4Client room = new Room4Client();
    public Room4Client ClientRoom { get { return room; } }

    protected override void StartOnlyOneTime()
    {
        room.Init();
        Room4Client.OnBeforeBattleBegin += OnBeforeBattleBegin;
        Room4Client.OnBattleBegin += OnBattleBegin;
        Room4Client.NotifyAddBattleUnit += OnAddBattleUnit;
        Room4Client.NotifyAddBuildingUnit += OnAddBuildingUnit;
        Room4Client.NotifyBuildingLevelUp += OnBuildingLevelUp;
        Room4Client.OnConstructingCompleted += OnConstructingCompleted;
        Room4Client.OnConstructingWaitingListChanged += OnConstructingWaitingListChanged;
        Room4Client.OnStartConstructingBattleUnit += OnStartConstructingBattleUnit;
        Room4Client.NotifyUnitRemoved += OnUnitRemoved;
        Room4Client.OnDoAttack += OnDoAttack;
        Room4Client.OnResourceProduced += OnResourceProduced;

        Room4Client.OnUsrsSwitched += OnUsrsSwitched;
    }

    private void OnResourceProduced(Unit u, string resType, Fix64 num)
    {
        if (resType != "Money")
            return;

        var mu = UC.GetModel(u.UID);
        if (num > 0)
            mu.AniPlayer.ConstructingUnit(); // 用建造单位动画表示资源生产
        else
            mu.AniPlayer.Idle();
    }

    private void OnUsrsSwitched()
    {
        RefreshVisions();
    }

    void RefreshVisions()
    {
        var me = GameCore.Instance.MePlayer;
        var mus = UC.AllModels();
        foreach (var mu in mus)
        {
            if (mu.U == null)
                continue;

            mu.IsMine = mu.U.Player == me;
            mu.WithVision = mu.U.cfg.VisionRadius > 0 && (mu.IsMine || mu.IsNeutral);
            mu.RefreshVision();
            mu.RefreshColor();
        }
    }

    private void OnDoAttack(Unit attacker, Unit target)
    {
        var attackDir = (target.Pos - attacker.Pos).Dir();
        string attack12 = null;
        Transform attackStub = null;
        var m = UC.GetModel(attacker.UID);
        if (target.cfg.IsAirForce)
        {
            attack12 = "Attack02";
            attackStub = m.Attack02Stub;
            if (m.AniPlayer != null)
                m.AniPlayer.AttackAir(attackDir, target.Pos);
        }
        else
        {
            attack12 = "Attack01";
            attackStub = m.Attack01Stub;
            if (m.AniPlayer != null)
                m.AniPlayer.AttackGround(attackDir, target.Pos);
        }

        var muTo = UC.GetModel(target.UID);
        if (attackStub != null && muTo != null)
            EC.CreateEffect(attacker.UnitType + attack12, attackStub, muTo.Root.position,
                (float)attacker.Room.GetPlayerUnitConfig(attacker.Player, attacker.UnitType).AttackInterval);
    }

    private void OnUnitRemoved(Unit u)
    {
        var uid = u.UID;
        IndLayer.DestroyBloodbar(uid);
        IndLayer.DestroyProgressbar(uid);
        IndLayer.DestroyWaitingNum(u.UID);
        IndLayer.DestroyRunningFlag(u.UID);

        // 部分单位立即移除，其它单位都是延迟移除，以便播放死亡动画
        if (u.cfg.NoBody || (u.cfg.IsBuilding && !u.BuildingCompleted))
            UC.RemoveModel(uid, 0);
        else
        {
            var mu = UC.GetModel(uid);
            if (mu != null && mu.AniPlayer != null)
                mu.AniPlayer.Die();

            UC.RemoveModel(uid, 1);
        }

        if (u.UnitType == "TreasureBox")
        {
            var type = u.Tag as string;
            UIManager.Instance.Tips.AddSmallTip(TreasureBoxRunner.GetDisplayName(type), u.Pos);
        }
    }

    private void OnStartConstructingBattleUnit(Unit u, string genType)
    {
        var m = UC.GetModel(u.UID);
        m.AniPlayer.ConstructingUnit();
        var constructingTime = u.Room.GetPlayerUnitConfig(u.Player, genType).ConstructingTime;
        IndLayer.CreateProgressbar(u, constructingTime).FollowTransformRoot = m.transform;
    }

    private void OnConstructingWaitingListChanged(Unit u, string genType)
    {
        // var wn = u.UnitCosntructingWaitingList.Count;
        var wn = u.UnitConstructed.Count;
        if (wn > 0)
            IndLayer.CreateWaitingNumber(u, wn);
        else
            IndLayer.DestroyWaitingNum(u.UID);
    }

    private void OnConstructingCompleted(Unit u)
    {
        var m = UC.GetModel(u.UID);
        m.U = u;
        m.AniPlayer.ConstructingComplete();
        IndLayer.DestroyProgressbar(u.UID);
        // IndLayer.DestroyWaitingNum(u.UID);
    }

    public void Clear()
    {
        UC.DestroyAll();
        MG.Clear();
        EC.Clear();
    }

    private void OnBeforeBattleBegin(string usr1, string usr2, Vec2 mapSize, bool inReplay)
    {
        Clear();
    }

    private void OnBattleBegin(Room4Client r, bool inReplay)
    {
    }

    private void OnAddBattleUnit(Unit building, Unit u)
    {
        if (building != null)
        {
            IndLayer.DestroyWaitingNum(building.UID);
            IndLayer.DestroyProgressbar(building.UID); // 可能是建造单位过程中显示的进度指示，这时可以去掉了
            UC.GetModel(building.UID).AniPlayer.ConstructingUnitComplete();
        }

        var m = CreateUnitModel(u);

        if (!u.cfg.Unattackable)
            IndLayer.CreateBloodbar(u).FollowTransformRoot = m.Root;

        if (!u.cfg.OutOfControl && u.cfg.MaxVelocity > 0)
            IndLayer.CreateRunningFlag(u).FollowTransformRoot = m.Root;
    }

    private void OnBuildingLevelUp(Unit u)
    {
        var m = UC.GetModel(u.UID);
        m.AniPlayer.LevelUpConstructing((float)u.cfg.ConstructingTime);
        IndLayer.CreateProgressbar(u, u.cfg.ConstructingTime).FollowTransformRoot = m.transform;
    }

    private void OnAddBuildingUnit(Unit u)
    {
        var m = CreateUnitModel(u);

        if (!u.cfg.Unattackable)
            IndLayer.CreateBloodbar(u).FollowTransformRoot = m.transform;

        if (u.BuildingCompleted)
            return;

        if (m.AniPlayer != null)
            m.AniPlayer.Construcing((float)u.cfg.ConstructingTime);

        IndLayer.CreateProgressbar(u, u.cfg.ConstructingTime).FollowTransformRoot = m.transform;
    }

    private void Update()
    {
        var dt = Time.deltaTime;
        EC.OnTimeElapsed(dt);
    }

    // 创建指定类型的单位模型
    MapUnit CreateUnitModel(Unit u)
    {
        var um = UC.CreateModel(u);
        um.transform.SetParent(Units, false);
        um.transform.localPosition = new Vector3((float)u.Pos.x, 0, (float)u.Pos.y);
        um.transform.localRotation = Quaternion.Euler(0, (float)u.Dir, 0);
        um.U = u;
        um.IsMine = GameCore.Instance.MePlayer == u.Player;
        um.MG = MG;
        um.gameObject.SetActive(true);

        return um;
    }
}
