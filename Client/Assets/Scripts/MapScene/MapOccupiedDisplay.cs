﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Swift;
using SCM;
using Swift.Math;

/// <summary>
/// 用于调试时显示地图位置占用情况
/// </summary>
public class MapOccupiedDisplay : MonoBehaviour
{
    public MapScene MS;

    List<Vec2> pts = new List<Vec2>();
    private void OnEnable()
    {
        pts.Clear();
        ReCaculate();
    }

    void ReCaculate()
    {
        pts.Clear();

        var scaler = (int)Map.GridSizeScaler;

        // scan the edges
        var w = (int)MS.ClientRoom.MapSize.x / scaler;
        var h = (int)MS.ClientRoom.MapSize.y / scaler;
        for (int x = 0; x < w; x++)
        {
            for (int y = 0; y < h; y++)
            {
                if (MS.ClientRoom.GetInGrid(x * scaler, y * scaler) <= 0)
                    continue;

                var pt = new Vec2(x, y);
                if (x == 0 || x == w - 1 || y == 0 || y == h - 1)
                {
                    if (!pts.Contains(pt))
                        pts.Add(pt);
                }
                else if (MS.ClientRoom.GetInGrid((x - 1) * scaler, y * scaler) == 0
                    || MS.ClientRoom.GetInGrid((x + 1) * scaler, y * scaler) == 0
                    || MS.ClientRoom.GetInGrid(x * scaler, (y - 1) * scaler) == 0
                    || MS.ClientRoom.GetInGrid(x * scaler, (y + 1) * scaler) == 0)
                    if (!pts.Contains(pt))
                        pts.Add(new Vec2(x, y));
            }
        }
    }

    private void OnDisable()
    {
        pts.Clear();
    }

#if UNITY_EDITOR

    void OnDrawGizmos()
    {
        var scaler = (int)Map.GridSizeScaler;

        var hw = (int)MS.ClientRoom.MapSize.x / 2;
        var hh = (int)MS.ClientRoom.MapSize.y / 2;
        Gizmos.color = Color.red;
        foreach (var center in pts)
        {
            Gizmos.DrawSphere(new Vector3((float)center.x * scaler - hw, 0,
                (float)center.y * scaler - hh) + transform.position, scaler);
        }
    }

#endif
}
