﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;

public class BattleResultUI : UIBase
{
    public Text ResultText;

    protected override void StartOnlyOneTime()
    {
        Room4Client.OnBattleEnd += OnBattleEnd;
    }

    private void OnBattleEnd(Room r, string winner, bool inReplay)
    {
        if (inReplay || GameCore.Instance.MePlayer == 0)
            return;

        var win = GameCore.Instance.MeID == winner;
        var rc = r as Room4Client;
        var award = win ? rc.WinnerAward : rc.LoserAward;
        var msg = award > 0 ? "获得 " + award + " 金币" : null;
        if (win && rc.WinnerAwardUnits != null)
        {
            var meInfo = GameCore.Instance.MeInfo;
            foreach (var c in rc.WinnerAwardUnits)
            {
                if (meInfo.GetCardLv(c) == 0)
                {
                    meInfo.AddUnit(c);
                    msg = (msg == null ? "获得 " : msg + ", ") + UnitConfiguration.GetDefaultConfig(c).DisplayName;
                }
            }
        }

        if (msg != null)
            UIManager.Instance.Tips.AddTip(msg);
    }
}
