﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;
using Swift.Math;

public class Progressbar : MonoBehaviour
{
    public Transform FollowTransformRoot;
    public Unit U;

    static float tanOblique = -1;
    void AdjustPos()
    {
        if (tanOblique < 0)
            tanOblique = Mathf.Tan(Camera.main.GetComponent<MainCamera>().Oblique);

        var delta = U.cfg.ShowSize * tanOblique + (U.cfg.IsAirForce ? 10 : 10);
        delta = GameCore.Instance.MePlayer == 1 ? delta : -delta;
        var sp = UIManager.Instance.World2IndicateUI(FollowTransformRoot.position + new Vector3(0, 0, (float)delta));
        RT.anchoredPosition = new Vector2((float)sp.x, (float)sp.y);
    }

    RectTransform RT;
    RectTransform ValueRT;
    GameObject Bg;
    GameObject Value;

    private void Start()
    {
        ValueRT = transform.Find("Value").GetComponent<RectTransform>();
        Bg = transform.Find("Bg").gameObject;
        RT = GetComponent<RectTransform>();
        Value = transform.Find("Value").gameObject;
    }

    Fix64 TotalTime;
    Fix64 timeElapsed;
    public void MoveForward(float te)
    {
        timeElapsed += te;
    }

    public void ResetProgress(Fix64 totalTime)
    {
        timeElapsed = 0;
        TotalTime = totalTime;
    }

    void Update()
    {
        if (U == null || FollowTransformRoot == null || TotalTime == 0)
        {
            Bg.SetActive(false);
            Value.SetActive(false);
            TotalTime = 0;
            return;
        }

        var p = timeElapsed / TotalTime;
        if (p >= 1 || p == 0)
        {
            Bg.SetActive(false);
            Value.SetActive(false);
        }
        else
        {
            Bg.SetActive(true);
            Value.SetActive(true);
            ValueRT.anchorMax = new Vector2((float)p, 1);
            AdjustPos();
        }
    }
}
