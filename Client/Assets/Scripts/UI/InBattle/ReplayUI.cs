﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using Swift.Math;

public class ReplayUI : UIBase, IPointerClickHandler {

    public RectTransform BC;
    public Image PrograssBar;
    public Text SpeedUpText;
    public MainCamera MC;

    const int speedUpMax = 8;
    int speedUp = 1;

    bool Showing = false;
    BattleReplayer br;

    public void ResetAll(bool inReplay)
    {
        br = GameCore.Instance.Get<BattleReplayer>();
        PrograssBar.fillAmount = 0;
        speedUp = 1;
        br.SpeedUpFactor = speedUp;
        SpeedUpText.text = speedUp + "X";
        MC.TurnOnBattleFog = !inReplay;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Show(!Showing);
    }

    public void Show(bool s)
    {
        Showing = s;
        waitTime = 0;
    }

    public void ChangeSpeedUp()
    {
        Show(true);
        var s = speedUp == 0 ? 1 : speedUp * 2;
        if (s > speedUpMax)
            s = 0;

        speedUp = s;
        SpeedUpText.text = s + "X";
        br.SpeedUpFactor = speedUp;
    }

    float waitTime = 0;
    private void Update()
    {
        waitTime += Time.deltaTime;

        if (Showing)
        {
            var y = BC.anchoredPosition.y;
            if (y >= 99)
                y = 100;
            else
            {
                var d = 100 - y;
                d /= 1.5f;
                y = 100 - d;
            }

            BC.anchoredPosition = new Vector3(0, y, 0);

            if (waitTime >= 3)
                Showing = false;
        }
        else
        {
            var y = BC.anchoredPosition.y;
            if (y <= 1)
                y = 0;
            else
                y /= 1.5f;

            BC.anchoredPosition = new Vector3(0, y, 0);
        }

        PrograssBar.fillAmount = br.Prograss;
    }
}
