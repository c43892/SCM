﻿using SCM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InBattleUI : UIBase {

    public GameObject SelAllBtn;

    protected override void StartOnlyOneTime()
    {
        Room4Client.OnBattleBegin += OnBattleBegin;
        Room4Client.OnBattleEnd += OnBattleEnd;
    }

    private void OnBattleBegin(Room4Client r, bool inReplay)
    {
        (ShowChildUI("ReplayUI", inReplay) as ReplayUI).ResetAll(inReplay);
        (ShowChildUI("StatisticsUI", true) as StatisticsUI).ResetAll();
        ShowChildUI("BattleResultUI", false);
        SelAllBtn.SetActive(!inReplay);
    }

    private void OnBattleEnd(Room r, string winner, bool inReplay)
    {
        var ui = ShowChildUI("BattleResultUI", true) as BattleResultUI;
        if (inReplay)
            ui.ResultText.text = "录像结束";
        else if (winner == null)
            ui.ResultText.text = "平局";
        else
            ui.ResultText.text = winner == GameCore.Instance.MeID ? "胜利" : "失败";

        SelAllBtn.SetActive(false);
    }

    public void OnReplay()
    {
        SelAllBtn.SetActive(false);
        var br = GameCore.Instance.Get<BattleReplayer>();
        br.Start();
    }

    public void HideAllChildren()
    {
        SelAllBtn.SetActive(false);
        ShowChildUI("ReplayUI", false);
        ShowChildUI("StatisticsUI", false);
        ShowChildUI("SelectUnitUI", false);
        ShowChildUI("BattleResultUI", false);
        ShowChildUI("BattleResultUI", false);
    }

    public void OnReturn()
    {
        UIManager.Instance.ClearScene();

        HideAllChildren();

        ShowTopUI("MainArea", true);
        ShowTopUI("MainMenu", true);
    }
}
