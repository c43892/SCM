﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;
using Swift.Math;

public class RunningFlag : MonoBehaviour
{
    public Transform FollowTransformRoot;
    public Unit U;

    static float tanOblique = -1;
    void AdjustPos()
    {
        if (U == null || FollowTransformRoot == null)
            return;

        if (tanOblique < 0)
            tanOblique = Mathf.Tan(Camera.main.GetComponent<MainCamera>().Oblique);

        var delta = U.cfg.ShowSize * tanOblique + (U.cfg.IsAirForce ? 35 : 35);
        delta = GameCore.Instance.MePlayer == 1 ? delta : -delta;
        var sp = UIManager.Instance.World2IndicateUI(FollowTransformRoot.position + new Vector3(0, 0, (float)delta));
        RT.anchoredPosition = new Vector2((float)sp.x, (float)sp.y);
    }

    RectTransform RT;
    Text txt;

    private void Start()
    {
        RT = GetComponent<RectTransform>();
        txt = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        txt.enabled = U != null && U.Hp > 0 && FollowTransformRoot != null && U.IgnoreTargetWhenMoving && U.MovePath.Count > 0;
        if (txt.enabled)
            AdjustPos();

        ////---- 临时放这里
        var art = U != null && U.Hp > 0 && FollowTransformRoot != null ? FollowTransformRoot.parent.Find("AttackRange") : null;
        if (art != null)
            art.gameObject.SetActive(!txt.enabled);
    }
}
