﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;
using Swift.Math;

public class Bloodbar : MonoBehaviour {

    public Transform FollowTransformRoot;
    public Unit U;

    static float tanOblique = -1;
    void AdjustPos()
    {
        if (U == null || FollowTransformRoot == null)
            return;

        if (tanOblique < 0)
            tanOblique = Mathf.Tan(Camera.main.GetComponent<MainCamera>().Oblique);

        var delta = U.cfg.ShowSize * tanOblique + (U.cfg.IsAirForce ? 20 : 20);
        delta = GameCore.Instance.MePlayer == 1 ? delta : -delta;
        var sp = UIManager.Instance.World2IndicateUI(FollowTransformRoot.position + new Vector3(0, 0, (float)delta));
        RT.anchoredPosition = new Vector2((float)sp.x, (float)sp.y);
    }

    RectTransform RT;
    RectTransform ValueRT;
    GameObject Bg;
    GameObject Value;

    private void Start()
    {
        ValueRT = transform.Find("Value").GetComponent<RectTransform>();
        Bg = transform.Find("Bg").gameObject;
        RT = GetComponent<RectTransform>();
        Value = transform.Find("Value").gameObject;
    }

    // Update is called once per frame
    void Update ()
    {
        if (U == null || FollowTransformRoot == null)
        {
            Bg.SetActive(false);
            Value.SetActive(false);
            return;
        }

        var p = (float)U.Hp / U.cfg.MaxHp;
        if (p >= 1 || p == 0)
        {
            Bg.SetActive(false);
            Value.SetActive(false);
        }
        else
        {
            Bg.SetActive(true);
            Value.SetActive(true);
            ValueRT.anchorMax = new Vector2(p, 1);
            AdjustPos();
        }
    }
}
