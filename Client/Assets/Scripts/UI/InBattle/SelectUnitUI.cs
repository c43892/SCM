﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;
using Swift.Math;

public class SelectUnitUI : UIBase {

    public Transform SelectArea;

    public Func<string, bool> IsChoiceValid;
    public Action<string> OnChoiceSel;

    public string[] Choices // 候选项
    {
        get
        {
            return choices;
        }
        set
        {
            choices = value;
        }
    } string[] choices = null;

    public string[] ChoicesName = null; // 候选项的显示文字

    public GameObject ChoiceBtn = null; // 选择按钮
    List<GameObject> choiceBtns = new List<GameObject>();

    // 建造位置
    public Vec2 Pos
    {
        get { return pos; }
        set
        {
            pos = value;
            SelectArea.transform.localPosition = new Vector3((float)pos.x, (float)pos.y, 0);
        }
    } Vec2 pos;
	
    // 点击空白处
    public void OnSelectNothing()
    {
        Hide();
    }

    public override void Hide()
    {
        foreach (var btn in choiceBtns)
            Destroy(btn.gameObject);

        choiceBtns.Clear();
        base.Hide();
    }

    // 点击某个选项
    void OnSelectOne(string choice)
    {
        if (IsChoiceValid != null && !IsChoiceValid(choice))
            return;

        Hide();
        OnChoiceSel.SC(choice);
    }

    // 重新排列所有选项
    public void Refresh()
    {
        // 销毁原有的按钮
        foreach (var btn in choiceBtns)
            Destroy(btn.gameObject);

        // 创建新按钮
        FC.For(choices.Length, (i) =>
        {
            var go = Instantiate(ChoiceBtn);
            go.transform.SetParent(SelectArea.transform);
            go.GetComponentInChildren<Text>().text = ChoicesName[i];
            var choiceTag = choices[i];
            go.GetComponent<Button>().onClick.AddListener(() => { OnSelectOne(choiceTag); });
            go.SetActive(true);
            go.transform.localRotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;
            choiceBtns.Add(go);
        });

        // 重排列所有按钮
        var cnt = choiceBtns.Count;

        if (cnt == 0)
        {
            Hide();
            return;
        }

        // 菜单显示尺寸
        var w = choiceBtns[0].GetComponent<RectTransform>().rect.width;
        var h = choiceBtns[0].GetComponent<RectTransform>().rect.height;
        if (cnt == 1)
            choiceBtns[0].transform.localPosition = Vector3.zero;
        else if (cnt == 2)
        {
            choiceBtns[0].transform.localPosition = new Vector3(-w * 0.75f, 0, 0);
            choiceBtns[1].transform.localPosition = new Vector3(w * 0.75f, 0, 0);
            w *= 2.5f;
        }
        else if (cnt == 3)
        {
            choiceBtns[0].transform.localPosition = new Vector3(-w, 0, 0);
            choiceBtns[1].transform.localPosition = new Vector3(0, 0, 0);
            choiceBtns[2].transform.localPosition = new Vector3(w, 0, 0);
            w *= 3;
        }
        else if (cnt < 6)
        {
            FC.For(cnt, (i) =>
            {
                var y = i < 3 ? h / 2 : -h / 2;
                var x = (i % 3) * w - w;
                choiceBtns[i].transform.localPosition = new Vector3(x, y, 0);
            });

            w *= 3;
            h *= 2;
        }
        else
        {
            FC.For(cnt, (i) =>
            {
                var y = i < 3 ? h : (i < 6 ? 0 : -h);
                var x = (i % 3) * w - w;
                choiceBtns[i].transform.localPosition = new Vector3(x, y, 0);
            });

            w *= 3;
            h *= 3;
        }

        // 检查边界调整位置
        var parentArea = SelectArea.parent.GetComponent<RectTransform>().rect;
        var uiPos = SelectArea.transform.localPosition;
        var uiPosX = uiPos.x.Clamp(w / 2, parentArea.width - w / 2);
        var uiPosY = uiPos.y.Clamp(h / 2, parentArea.height - h / 2);
        SelectArea.transform.localPosition = new Vector3(uiPosX, uiPosY, 0);
    }
}
