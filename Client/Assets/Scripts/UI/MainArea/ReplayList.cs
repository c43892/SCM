﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;

public class ReplayList : UIBase {

    public GameObject[] Btns;

    public string[] Replays
    {
        set
        {
            replays = value;

            foreach (var btn in Btns)
                btn.SetActive(false);

            if (replays == null)
                return;

            for (var i = 0; i < Btns.Length && i < replays.Length; i++)
            {
                Btns[i].SetActive(true);

                var replayID = replays[i];
                var n = replayID.IndexOf('.'); // skip No
                n = replayID.IndexOf('.', n + 1); // skip pvp flag
                n = replayID.IndexOf('.', n + 1); // skip date
                n = replayID.IndexOf('.', n + 1); // skip time
                var showText = replayID.Substring(n + 1).Replace(".vs.", " vs ");
                Btns[i].GetComponentInChildren<Text>().text = showText;
            }
        }
    } string[] replays = null;

    public void OnGetReplay(int i)
    {
        var conn = GameCore.Instance.ServerConnection;
        var buff = conn.Request2Srv("GetReplay", (data) =>
        {
            var exists = data.ReadBool();
            if (!exists)
            {
                AddTip("没有找到录像");
                return;
            }

            UIManager.Instance.ShowTopUI("ReplayUI", true);
            var replayer = GameCore.Instance.Get<BattleReplayer>();
            replayer.Clear();
            replayer.ReadFromBuffer(data);
            replayer.Start();
        });
        buff.Write(replays[i]);
        conn.End(buff);
    }
}
