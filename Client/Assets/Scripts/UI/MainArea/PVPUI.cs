﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PVPUI : UIBase
{
    public GameObject WaitingMatchPVP;

    public Text Name;
    public Text WinCount;
    public Text LoseCount;
    public Text Rate;

    public override void Show()
    {
        base.Show();
        WaitingMatchPVP.SetActive(false);

        var meInfo = GameCore.Instance.MeInfo;
        Name.text = GameCore.Instance.MeID;
        WinCount.text = meInfo.WinCount.ToString();
        LoseCount.text = meInfo.LoseCount.ToString();
        var total = meInfo.WinCount + meInfo.LoseCount;
        Rate.text = total <= 0 ? " - " : (int)(meInfo.WinCount * 100 / total) + "%";
    }

    // 匹配对手
    public void OnSelPVP(bool ranking)
    {
        if (ranking)
        {
            AddTip("暂未开放大师排名系统，请打练习赛");
            return;
        }

        // 发送匹配请求
        var conn = GameCore.Instance.ServerConnection;
        var buff = conn.Send2Srv("MatchIn");
        buff.Write(ranking);
        conn.End(buff);
        WaitingMatchPVP.SetActive(true);
    }

    // 取消匹配
    public void OnCancelPVP()
    {
        // 发送匹配请求
        var conn = GameCore.Instance.ServerConnection;
        conn.End(conn.Request2Srv("CancelMatchIn", (data) =>
        {
            var canceled = data.ReadBool();
            if (canceled)
                WaitingMatchPVP.SetActive(false);
        }));
    }
}
