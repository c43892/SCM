﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;

public class CardUI : UIBase
{
    public Text Name;
    public Text Lv;
    public Text Num;
    public Image CombineOp;
    public Image Avatar;
    public Text Prerequisite;
    public Text Attrs;

    public string UnitType
    {
        get
        {
            return unitType;
        }
        set
        {
            unitType = value;
            RefreshCardInfo();
        }
    } string unitType;

    public Color GrayColor = new Color(0.7f, 0.7f, 0.7f, 1);
    void RefreshCardInfo()
    {
        var meInfo = GameCore.Instance.MeInfo;
        var num = meInfo.Cards[unitType].Key;
        var lv = meInfo.Cards[unitType].Value;
        var cfg = UnitConfiguration.GetConfig(unitType, lv);

        Name.text = cfg.DisplayName;
        if (unitType != "RadarSign" && unitType != "SoldierCarrier")
        {
            Name.text += "(";
            if (cfg.IsAirForce)
                Name.text += " 空中";
            if (cfg.IsBiological)
                Name.text += " 生物";
            if (cfg.IsMechanical)
                Name.text += " 机械";
            if (cfg.IsBuilding)
                Name.text += " 建筑";
            Name.text += " )";
        }

        Lv.text = "Lv: " + lv.ToString();
        Lv.enabled = lv >= 1;
        Num.text = cfg.CardsCost == null || lv >= cfg.CardsCost.Length ?
            "Max" : num.ToString() + "/" + cfg.CardsCost[lv].ToString();
        CombineOp.color = cfg.CardsCost == null || lv >= cfg.CardsCost.Length || num >= cfg.CardsCost[lv] ?
            Color.green : GrayColor;

        Avatar.color = lv > 0 ? Color.white : GrayColor;

        Prerequisite.text = "无";
        Prerequisite.color = Color.green;

        // 只显示第一个前置需求
        if (cfg.Prerequisites != null)
        {
            Prerequisite.text = "";
            var c = cfg.Prerequisites[0];
            Prerequisite.color = meInfo.GetCardLv(c) >= 1 ? Color.green : Color.red;
            Prerequisite.text = UnitConfiguration.GetDefaultConfig(c).DisplayName;
        }

        // 有该单位就显示属性
        var attrsTxt = "";
        attrsTxt += "生命: " + cfg.MaxHp + "\r\n";
        if (cfg.Power != 0 && cfg.AttackInterval > 0)
        {
            attrsTxt += "攻击力: " + cfg.Power + "\r\n";
            attrsTxt += "攻击类型: " + ((cfg.CanAttackGround && cfg.CanAttackAir ? "对空, 对地" :
                cfg.CanAttackGround ? "对地" : "对空")) + "\r\n";
            attrsTxt += "攻速: " + (((int)(1 / cfg.AttackInterval) * 10) / 10.0f) + "\r\n";
        }
        attrsTxt += "防御: " + cfg.Defence + "\r\n";
        attrsTxt += "速度: " + (cfg.MaxVelocity / 10.0f);
        
        Attrs.text = attrsTxt;
    }

    // 合成或者升级按钮
    public void OnCombineOrUpgrade()
    {
        var info = GameCore.Instance.MeInfo;
        var c = unitType;
        var lv = info.GetCardLv(c);
        var cfg = UnitConfiguration.GetConfig(c, lv);

        // 已经到最高级
        if (cfg.CardsCost == null || lv >= cfg.CardsCost.Length)
        {
            AddTip("已经达到最高级");
            return;
        }

        var cardCost = cfg.CardsCost[lv];
        if (info.GetCardNum(c) < cardCost)
        {
            AddTip("卡片数量不足");
            return;
        }

        var CM = GameCore.Instance.Get<CardManager>();
        CM.RequestCombineOrUpgrade(unitType, (ok) =>
        {
            if (!ok)
            {
                AddTip((lv == 0 ? "合成" : "升级") + "失败");
                return;
            }

            RefreshCardInfo();
            AddTip((lv == 0 ? "合成" : "升级") + "成功");
        });
    }
}
