﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;

public class PVELvUI : SCMBehaviour
{
    public Text Title;
    public Text Desc;
    public Level Lv
    {
        get
        {
            return lv;
        }
        set
        {
            lv = value;
            if (lv == null)
                return;

            Title.text = lv.DisplayName;
            Desc.text = lv.Description;
        }
    } Level lv;

    public void OnBtnClick()
    {
        // 发送匹配请求
        var conn = GameCore.Instance.ServerConnection;
        var buff = conn.Send2Srv("MatchRobot");
        buff.Write(lv.LevelID);
        conn.End(buff);
    }
}
