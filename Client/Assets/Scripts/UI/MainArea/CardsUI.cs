﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;

public class CardsUI : UIBase
{
    public Text Coins;
    public Transform ContentArea;
    public GameObject CardModel;

    List<GameObject> cardGos = new List<GameObject>();

    public override void Show()
    {
        base.Show();
        Refresh();
    }

    void ClearAllCards()
    {
        foreach (var go in cardGos)
            Destroy(go);

        cardGos.Clear();
    }

    public void Refresh()
    {
        var meInfo = GameCore.Instance.MeInfo;

        Coins.text = meInfo.Coins.ToString();

        ClearAllCards();
        foreach (var ct in meInfo.Cards.KeyArray)
        {
            var go = Instantiate(CardModel);
            go.SetActive(true);
            go.transform.SetParent(ContentArea);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
            cardGos.Add(go);
            go.GetComponent<CardUI>().UnitType = ct;
        }
    }
    
    public void OnDrawCard()
    {
        var meInfo = GameCore.Instance.MeInfo;
        if (meInfo.Coins < CardManager.DrawCardCost)
        {
            AddTip("金币不足");
            return;
        }

        var CM = GameCore.Instance.Get<CardManager>();
        CM.RequestDrawCard((ok, c) =>
        {
            if (!ok)
            {
                AddTip("抽卡失败");
                return;
            }

            AddTip("恭喜获得：" + UnitConfiguration.GetDefaultConfig(c).DisplayName);
            Refresh();
        });
    }
}
