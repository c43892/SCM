﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Swift;
using SCM;

public class PVEUI : UIBase
{
    public PVELvUI PVEModel;
    StableDictionary<string, PVELvUI> pves = new StableDictionary<string, PVELvUI>();

    bool inited = false;
    public override void Show()
    {
        if (!inited)
        {
            foreach (var lv in LevelCreator.AllPVELevels)
            {
                var pve = Instantiate(PVEModel) as PVELvUI;
                pve.transform.SetParent(PVEModel.transform.parent);
                pve.transform.localPosition = Vector3.zero;
                pve.Lv = lv;
                pves[lv.LevelID] = pve;
            }

            inited = true;
        }

        var meInfo = GameCore.Instance.MeInfo;
        foreach (var pve in pves.ValueArray)
        {
            pve.gameObject.SetActive(true);
            pve.GetComponent<Image>().color = meInfo.PassedPVE(pve.Lv.LevelID) ? Color.Lerp(Color.green, Color.white, 0.85f) : Color.white;
        }

        base.Show();
    }
}
