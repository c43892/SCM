﻿using SCM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainAreaUI : UIBase {

    protected override void StartOnlyOneTime()
    {
        Room4Client.OnBattleBegin += OnBattleBegin;
    }

    private void OnBattleBegin(Room4Client r, bool inReplay)
    {
        Hide();
    }
}
