﻿using SCM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : UIBase {

    public RectTransform PVEBtn;
    public RectTransform PVPBtn;
    public RectTransform CardsBtn;
    public RectTransform ReplaysBtn;
    public RectTransform CurSelMark;

    public UIBase PVEUI;
    public UIBase PVPUI;
    public UIBase CardsUI;
    public UIBase RecentReplaysUI;

    protected override void StartOnlyOneTime()
    {
        Room4Client.OnBattleBegin += OnBattleBegin;
    }

    public override void Show()
    {
        base.Show();
        OnSelPVE();
    }

    private void OnBattleBegin(Room4Client r, bool inReplay)
    {
        Hide();
    }

    void HideAll()
    {
        PVEUI.Hide();
        PVPUI.Hide();
        CardsUI.Hide();
        RecentReplaysUI.Hide();
    }

    public void OnSelPVE()
    {
        HideAll();
        PVEUI.Show();
        CurSelMark.anchorMin = PVEBtn.anchorMin;
        CurSelMark.anchorMax = PVEBtn.anchorMax;
    }

    public void OnSelPVP()
    {
        if (!GameCore.Instance.MeInfo.PassedPVE("Training00"))
        {
            AddTip("请先战胜 指引关卡");
            return;
        } else if(!GameCore.Instance.MeInfo.PassedPVE("Training01"))
        {
            AddTip("请先战胜 训练关卡");
            return;
        }

        HideAll();
        PVPUI.Show();
        CurSelMark.anchorMin = PVPBtn.anchorMin;
        CurSelMark.anchorMax = PVPBtn.anchorMax;
    }

    public void OnSelCards()
    {
        HideAll();
        CardsUI.Show();
        CurSelMark.anchorMin = CardsBtn.anchorMin;
        CurSelMark.anchorMax = CardsBtn.anchorMax;
    }

    public void OnReplays()
    {
        HideAll();
        RecentReplaysUI.Show();
        CurSelMark.anchorMin = ReplaysBtn.anchorMin;
        CurSelMark.anchorMax = ReplaysBtn.anchorMax;

        var rl = RecentReplaysUI.GetComponentInChildren<ReplayList>();
        var conn = GameCore.Instance.ServerConnection;
        var buff = conn.Request2Srv("GetReplayList", (data) =>
        {
            var arr = data.ReadStringArr();
            rl.Replays = arr;
            if (arr == null || arr.Length == 0)
            {
                AddTip("未找到最近录像");
                return;
            }
        });
        buff.Write(5);
        conn.End(buff);
    }
}
