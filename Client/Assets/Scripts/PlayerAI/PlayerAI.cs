﻿using System;
using System.Collections.Generic;
using Swift;
using SCM;
using Swift.Math;

namespace SCM
{
    /// <summary>
    /// 电脑对手 AI
    /// </summary>
    public abstract class PlayerAI
    {
        protected int p;
        protected StateMachine sm;

        public static Action<int, Type> ConstructBuilding;
        public static Action<Unit, string> ConstructBattleUnit;
        public static Action<Unit, Vec2> Move2;

        public PlayerAI(StateMachine stateMachine)
        {
            sm = stateMachine;
        }

        // 创建 AI 状态机
        public abstract void BuildAI();
    }
}
