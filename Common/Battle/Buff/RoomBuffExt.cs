﻿using System;
using System.Collections.Generic;
using Swift;
using Swift.Math;

namespace SCM
{
    /// <summary>
    /// 扩展 Room 的 Buff 行为
    /// </summary>
    public static class RoomBuffExt
    {
        public static void CreateBuff(this Room room, int player, string buffType)
        {
            Buff b = null;
            switch(buffType)
            {
                case "AddBiologyPower":
                    b = AddBiologyPower();
                    break;
                case "AddBiologyDefence":
                    b = AddBiologyDefence();
                    break;
                case "AddMachinePower":
                    b = AddMachinePower();
                    break;
                case "AddMachineDefence":
                    b = AddMachineDefence();
                    break;
            }

            if (b != null)
            {
                b.Type = buffType;
                room.BuffRunner.AddBuff(player, b);
            }
        }

        // 增加生物单位攻击
        static Buff AddBiologyPower()
        {
            Func<Unit, Fix64> calcDA = (u) =>
            {
                if (!u.cfg.IsBiological)
                    return 0;

                var da = u.cfg.Power / 10;
                if (da < 1)
                    da = 1;

                return da;
            };

            var b = new Buff();
            b.OnUnit = (u) => { u.PowerAdd += calcDA(u); };
            b.OffUnit = (u) => { u.PowerAdd -= calcDA(u); };

            return b;
        }

        // 增加生物单位防御
        static Buff AddBiologyDefence()
        {
            var b = new Buff();
            b.OnUnit = (u) => { if (!u.cfg.IsBiological) return; u.DefenceAdd += 1; };
            b.OffUnit = (u) => { if (!u.cfg.IsBiological) return; u.DefenceAdd -= 1; };

            return b;
        }

        // 增加机械单位攻击
        static Buff AddMachinePower()
        {
            Func<Unit, Fix64> calcDA = (u) =>
            {
                if (!u.cfg.IsMechanical)
                    return 0;

                var da = u.cfg.Power / 10;
                if (da < 1)
                    da = 1;

                return da;
            };

            var b = new Buff();
            b.OnUnit = (u) => { u.PowerAdd += calcDA(u); };
            b.OffUnit = (u) => { u.PowerAdd -= calcDA(u); };

            return b;
        }

        // 增加机械单位防御
        static Buff AddMachineDefence()
        {
            var b = new Buff();
            b.OnUnit = (u) => { if (u.cfg.IsMechanical) return; u.DefenceAdd += 1; };
            b.OffUnit = (u) => { if (u.cfg.IsMechanical) return; u.DefenceAdd -= 1; };

            return b;
        }
    }
}
