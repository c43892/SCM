﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Swift;
using Swift.Math;

namespace SCM
{
    /// <summary>
    /// 普通 PVP 关卡
    /// </summary>
    public class LvPVP : Level
    {
        public override string LevelID { get { return "PVP"; } }
        public override string DisplayName { get { return "对战地图"; } }
        public override string Description { get { return "用于普通 PVP 对战"; } }
        public override bool IsPVP { get { return true; } }

        public override void InitResource(Room r)
        {
            r.AddResource(1, "Money", 1000);
            r.AddResource(2, "Money", 1000);
        }

        public override int CheckWinner(Room r)
        {
            if (!r.ExistsUnit((u) => u.cfg.IsBuilding && u.Player == 1))
                return 2;
            else if (!r.ExistsUnit((u) => u.cfg.IsBuilding && u.Player == 2))
                return 1;
            else
                return -1;
        }

        Vec2[] poses = null;
        Fix64 ccOffset = Fix64.Zero;
        public override void InitMapSetting(Room r)
        {
            // 矿点位置
            ccOffset = UnitConfiguration.GetDefaultConfig("CommanderCenter").RealSize + 50;
            poses = new Vec2[]
            {
                new Vec2(MapHalfSize.x, ccOffset),
                new Vec2(MapHalfSize.x / 3 - 25, MapHalfSize.y / 2 - 150),
                new Vec2(MapHalfSize.x * 1.75 + 25, MapHalfSize.y / 2 - 200),
                new Vec2(MapHalfSize.x - 150, MapHalfSize.y + 50),
            };

            // 初始化地图矿点
            foreach (var pos in poses)
            {
                r.AddNewUnit(null, "CCStub", pos, 0);
                r.AddNewUnit(null, "CCStub", MapSize - pos, 0);
            }

            // 初始化地图障碍
            r.AddObstacle(Vec2.Zero, new Vec2(MapSize.x, 0));
            r.AddObstacle(Vec2.Zero, new Vec2(0, MapSize.y));
            r.AddObstacle(MapSize, new Vec2(MapSize.x, 0));
            r.AddObstacle(MapSize, new Vec2(0, MapSize.y));

            //var p1 = new Vec2(MapSize.x / 2 - 150, MapSize.y);
            //var p2 = p1 + new Vec2(50, -250);
            //r.AddObstacle(p1, p2);
            //r.AddObstacle(MapSize - p1, MapSize - p2);
            //p1 = p2;
            //p2 = p1 + new Vec2(200, -75);
            //r.AddObstacle(p1, p2);
            //r.AddObstacle(MapSize - p1, MapSize - p2);
            //p1 = p2;
            //p2 = p1 + new Vec2(25, -25);
            //r.AddObstacle(p1, p2);
            //r.AddObstacle(MapSize - p1, MapSize - p2);
            //p1 = MapSize + new Vec2(0, -400);
            //p2 = p1 + new Vec2(-100, 25);
            //r.AddObstacle(p1, p2);
            //r.AddObstacle(MapSize - p1, MapSize - p2);
        }

        public override void InitBuilding(Room r)
        {
            // 初始基地和治疗塔
            var cc1 = r.AddNewUnit(null, "CommanderCenter", poses[0], 1);
            cc1.BuildingCompleted = true;
            cc1.Hp = cc1.cfg.MaxHp;
            var ht1 = r.AddNewUnit(null, "HealingTower", poses[0], 1);
            ht1.BuildingCompleted = true;
            ht1.Hp = ht1.cfg.MaxHp;
            var cc2 = r.AddNewUnit(null, "CommanderCenter", MapSize - poses[0], 2);
            cc2.BuildingCompleted = true;
            cc2.Hp = cc2.cfg.MaxHp;
            var ht2 = r.AddNewUnit(null, "HealingTower", MapSize - poses[0], 2);
            ht2.BuildingCompleted = true;
            ht2.Hp = ht2.cfg.MaxHp;

            // 开局临时视野
            r.AddNewUnit(null, "TempVisionAtBeginning", MapHalfSize, 0).Hp = 5;
        }

        public override void InitBattleUnits(Room r)
        {
            // 开局各送一个机枪兵
            var stc1 = r.AddNewUnit(null, "SoldierCarrier", new Vec2(-200, ccOffset * 2), 1);
            stc1.UnitCosntructingWaitingList.Add("Soldier");
            stc1.Dir = 0;
            stc1.MovePath.Add(new Vec2(MapHalfSize.x, ccOffset * 2));
            stc1.MovePath.Add(new Vec2(MapSize.x + 200, ccOffset * 2));

            var stc2 = r.AddNewUnit(null, "SoldierCarrier", new Vec2(MapSize.x + 200, MapSize.y - ccOffset * 2), 2);
            stc2.UnitCosntructingWaitingList.Add("Soldier");
            stc2.Dir = 180;
            stc2.MovePath.Add(new Vec2(MapHalfSize.x, MapSize.y - ccOffset * 2));
            stc2.MovePath.Add(new Vec2(-200, MapSize.y - ccOffset * 2));
        }
    }
}
