﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Swift;
using Swift.Math;

namespace SCM
{
    public class LvPVE001 : Level
    {
        public override string LevelID { get { return "PVE001"; } }
        public override string DisplayName { get { return "逃出帝国"; } }
        public override bool IsPVP { get { return false; } }
        public override string Description
        {
            get
            {
                return
                    "<color=#00000000>口口</color>皇室两兄弟已经彻底决裂，哥哥宣布老国王已将王位传给自己，弟弟大刘要求面见老国王遭到拒绝，随带领亲信部队发动政变。但皇宫早有准备，政变失败，弟弟被迫逃离帝国"
                    + "\r\n\r\n" +
                    "<color=#00000000>口口</color>你需要帮助大刘在边境暂时驻扎基地，并清理帝国巡逻部队";
            }
        }

        public override void InitResource(Room r)
        {
            r.AddResource(2, "Money", 100);
        }

        Vec2[] poses = null;
        Fix64 ccOffset = Fix64.Zero;
        public override void InitMapSetting(Room r)
        {
            // 矿点位置
            ccOffset = UnitConfiguration.GetDefaultConfig("CommanderCenter").RealSize + 50;
            poses = new Vec2[]
            {
                new Vec2(MapHalfSize.x, ccOffset),
            };

            // 初始化地图矿点
            foreach (var pos in poses)
            {
                r.AddNewUnit(null, "CCStub", pos, 0);
                r.AddNewUnit(null, "CCStub", MapSize - pos, 0);
            }
        }

        public override void InitBuilding(Room r)
        {
            // 双方地方基地
            var cc1 = r.AddNewUnit(null, "CommanderCenter", poses[0], 1);
            cc1.BuildingCompleted = true;
            cc1.Hp = cc1.cfg.MaxHp;
            var cc2 = r.AddNewUnit(null, "CommanderCenter", MapSize - poses[0], 2);
            cc2.BuildingCompleted = true;
            cc2.Hp = cc2.cfg.MaxHp;

            // 临时视野
            r.AddNewUnit(null, "TempVisionAtBeginning", Vec2.Zero, 0).Hp = 1;
        }

        // 胜利条件为基地被拆
        public override int CheckWinner(Room r)
        {
            if (r.GetUnitsByType("CommanderCenter", 1).Length == 0)
                return 2;
            else if (r.GetUnitsByType("CommanderCenter", 2).Length == 0)
                return 1;
            else
                return -1;
        }

        public override void InitBattleUnits(Room r)
        {
            // 本方矿点，敌人部队
            r.AddNewUnit(null, "Soldier", MapSize - poses[0] + new Vec2(70, -70), 1);

            // 对方矿点，敌人部队
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(60, 50), 1);
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(0, 70), 1);
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(-60, 50), 1);

            // 本方初始部队
            r.AddNewUnit(null, "Soldier", MapHalfSize + new Vec2(200, 0), 2);
            r.AddNewUnit(null, "Soldier", MapHalfSize + new Vec2(200, 0), 2);
        }
    }
}
