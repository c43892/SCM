﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Swift;
using Swift.Math;

namespace SCM
{
    public class LvPVE003 : Level
    {
        public override string LevelID { get { return "PVE003"; } }
        public override string DisplayName { get { return "全面反攻"; } }
        public override bool IsPVP { get { return false; } }
        public override string Description
        {
            get
            {
                return
                    "<color=#00000000>口口</color>小诺突袭了皇宫卫队，救出被软禁的老国王。现在就是大举反击的最好时机"
                    + "\r\n\r\n" +
                    "<color=#00000000>口口</color>防守皇宫的炮台和坦克非常厉害，但都无法对空。我们可以使用坦克强攻，但空军会事半功倍";
            }
        }

        public override void InitResource(Room r)
        {
            r.AddResource(2, "Money", 1000);
        }

        Vec2[] poses = null;
        Fix64 ccOffset = Fix64.Zero;
        public override void InitMapSetting(Room r)
        {
            // 矿点位置
            ccOffset = UnitConfiguration.GetDefaultConfig("CommanderCenter").RealSize + 50;
            poses = new Vec2[]
            {
                new Vec2(MapHalfSize.x, ccOffset),
            };

            // 初始化地图矿点
            foreach (var pos in poses)
            {
                r.AddNewUnit(null, "CCStub", pos, 0);
                r.AddNewUnit(null, "CCStub", MapSize - pos, 0);
            }
        }

        public override void InitBuilding(Room r)
        {
            // 双方地方基地
            var cc1 = r.AddNewUnit(null, "CommanderCenter", poses[0], 1);
            cc1.BuildingCompleted = true;
            cc1.Hp = 50;
            var cc2 = r.AddNewUnit(null, "CommanderCenter", MapSize - poses[0], 2);
            cc2.BuildingCompleted = true;
            cc2.Hp = cc2.cfg.MaxHp;

            // 临时视野
            r.AddNewUnit(null, "TempVisionAtBeginning", Vec2.Zero, 0).Hp = 1;

            // 本方工厂和星港
            var brk = r.AddNewUnit(null, "Barrack", MapSize - poses[0] + new Vec2(150, 0), 2);
            brk.BuildingCompleted = true;
            brk.Hp = brk.cfg.MaxHp;
            var fct = r.AddNewUnit(null, "Factory", MapSize - poses[0] + new Vec2(100, -100), 2);
            fct.BuildingCompleted = true;
            fct.Hp = fct.cfg.MaxHp;
            var stp = r.AddNewUnit(null, "Airport", MapSize - poses[0] + new Vec2(-100, -100), 2);
            stp.BuildingCompleted = true;
            stp.Hp = stp.cfg.MaxHp;

            // 皇宫围墙
            var obvs = new Vec2[15];
            FC.For(15, (i) =>
            {
                var arc = i * MathEx.Pi / 32;
                obvs[i] = new Vec2(MathEx.Cos(arc) * 300 + MapHalfSize.x, MathEx.Sin(arc) * 300);
            });
            r.AddObstacle(obvs);

            FC.For(15, (i) =>
            {
                var arc = (i + 1) * MathEx.Pi / 32 + MathEx.HalfPi;
                obvs[i] = new Vec2(MathEx.Cos(arc) * 300 + MapHalfSize.x, MathEx.Sin(arc) * 300);
            });
            r.AddObstacle(obvs);

            // 皇宫炮台
            var bkrPos = new Vec2[] {
                new Vec2(MapHalfSize.x, 250),
                new Vec2(MapHalfSize.x - 75, 250),
                new Vec2(MapHalfSize.x + 75, 250),
                new Vec2(MapHalfSize.x - 150, 200),
                new Vec2(MapHalfSize.x + 150, 200),
            };
            foreach (var p in bkrPos)
                r.AddNewUnit(null, "BigBunker", p, 1);
        }

        // 胜利条件为基地被拆
        public override int CheckWinner(Room r)
        {
            if (r.GetUnitsByType("CommanderCenter", 1).Length == 0)
                return 2;
            else if (r.GetUnitsByType("CommanderCenter", 2).Length == 0)
                return 1;
            else
                return -1;
        }

        public override void InitBattleUnits(Room r)
        {
            // 敌人部队
            r.AddNewUnit(null, "Tank", poses[0] + new Vec2(-100, 250), 1);
            r.AddNewUnit(null, "Tank", poses[0] + new Vec2(0, 250), 1);
            r.AddNewUnit(null, "Tank", poses[0] + new Vec2(100, 250), 1);

            // 我方部队
            r.AddNewUnit(null, "Soldier", new Vec2(MapHalfSize.x - 50, MapSize.y - 350), 2);
            r.AddNewUnit(null, "Soldier", new Vec2(MapHalfSize.x + 50, MapSize.y - 350), 2);
            var stc2 = r.AddNewUnit(null, "SoldierCarrier", new Vec2(MapSize.x + 200, MapSize.y - 300), 2);
            stc2.UnitCosntructingWaitingList.Add("Tank");
            stc2.Dir = 180;
            stc2.MovePath.Add(new Vec2(MapHalfSize.x, MapSize.y - 300));
            stc2.MovePath.Add(new Vec2(-200, MapSize.y - 300));
        }
    }
}
