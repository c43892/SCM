﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Swift;
using Swift.Math;

namespace SCM
{
    public class LvPVE002 : Level
    {
        public override string LevelID { get { return "PVE002"; } }
        public override string DisplayName { get { return "守卫阵地"; } }
        public override bool IsPVP { get { return false; } }
        public override string Description
        {
            get
            {
                return
                    "<color=#00000000>口口</color>帝国派出边境卫队围剿大刘驻地，形势危急。但狙击手小诺发现了新国王不可告人的秘密，并决定潜入帝国后方，帮助我们"
                    + "\r\n\r\n" +
                    "<color=#00000000>口口</color>炮台是防守利器，守住前线，再派狙击手绕路潜入敌人后方，摧毁敌方基地";
            }
        }

        public override void InitResource(Room r)
        {
            r.AddResource(2, "Money", 1000);
        }

        Vec2[] poses = null;
        Fix64 ccOffset = Fix64.Zero;
        public override void InitMapSetting(Room r)
        {
            // 矿点位置
            ccOffset = UnitConfiguration.GetDefaultConfig("CommanderCenter").RealSize + 50;
            poses = new Vec2[]
            {
                new Vec2(MapHalfSize.x, ccOffset),
            };

            // 初始化地图矿点
            foreach (var pos in poses)
            {
                r.AddNewUnit(null, "CCStub", pos, 0);
                r.AddNewUnit(null, "CCStub", MapSize - pos, 0);
            }
        }

        public override void InitBuilding(Room r)
        {
            // 双方地方基地
            var cc1 = r.AddNewUnit(null, "CommanderCenter", poses[0], 1);
            cc1.BuildingCompleted = true;
            cc1.Hp = 50;
            var cc2 = r.AddNewUnit(null, "CommanderCenter", MapSize - poses[0], 2);
            cc2.BuildingCompleted = true;
            cc2.Hp = cc2.cfg.MaxHp;

            // 对方兵营
            var brk1 = r.AddNewUnit(null, "Barrack", MapSize / 2 + new Vec2(100, 0), 1);
            brk1.BuildingCompleted = true;
            brk1.Hp = brk1.cfg.MaxHp;

            // 临时视野
            r.AddNewUnit(null, "TempVisionAtBeginning", Vec2.Zero, 0).Hp = 1;

            // 本方兵营和炮台
            var brk2 = r.AddNewUnit(null, "Barrack", MapSize - poses[0] + new Vec2(100, -100), 2);
            brk2.BuildingCompleted = true;
            brk2.Hp = brk2.cfg.MaxHp;
            var bck1 = r.AddNewUnit(null, "Bunker", MapSize - poses[0] + new Vec2(-100, -200), 2);
            var bck2 = r.AddNewUnit(null, "Bunker", MapSize - poses[0] + new Vec2(0, -200), 2);
            var bck3 = r.AddNewUnit(null, "Bunker", MapSize - poses[0] + new Vec2(100, -200), 2);
        }

        // 胜利条件为基地被拆
        public override int CheckWinner(Room r)
        {
            if (r.GetUnitsByType("CommanderCenter", 1).Length == 0)
                return 2;
            else if (r.GetUnitsByType("CommanderCenter", 2).Length == 0)
                return 1;
            else
                return -1;
        }

        public override void InitBattleUnits(Room r)
        {
            // 敌人部队
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(30, 150), 1);
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(-30, 150), 1);
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(-50, 250), 1);
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(0, 250), 1);
            r.AddNewUnit(null, "Soldier", poses[0] + new Vec2(50, 250), 1);
        }
    }
}
