﻿using System;
using System.Collections.Generic;
using Swift;
using Swift.Math;
using System.Linq;

namespace SCM
{
    /// <summary>
    /// 单位配置信息
    /// </summary>
    public class UnitConfigInfo
    {
        public string DisplayName; // 单位显示名称
        public int Cost; // 建造花费
        public Fix64 ConstructingTime; // 建造时间
        public int MaxHp; // 最大血量
        public Fix64 ShowSize; // 显示尺寸
        public Fix64 RealSize; // 站位尺寸半径
        public Fix64 VisionRadius; // 视野半径
        public Fix64 GuardingRange; // 警戒距离
        public bool NeutralType; // 中立类型

        public string[] GenUnitTypes; // 作战单位生产类型
        public string[] GenResourceType; // 资源生产类型
        public Fix64[] GenResourceRate; // 资源生产速率/秒
        public string[] LevelUps; // 可升级的目标类型
        public string LevelUpFrom; // 从哪个类型升级得到

        public Fix64 AttackRange; // 攻击范围
        public Fix64 MaxVelocity; // 移动速率
        public Fix64 Power; // 攻击力
        public string PowerType; // 攻击力计算类型, ByValue, ByPrecentage
        public bool CanAttackGround; // 是否可以对地
        public bool CanAttackAir; // 是否可以对空
        public Fix64 Defence; // 防御
        public Fix64 AttackInterval; // 攻击间隔
        public string AOEAreaType; // 范围攻击区域类型 (Fan, Line, Pie)
        public Fix64[] AOEParams; // 范围攻击区域参数

        public string ChangeMode; // 模式切换时的目标类型
        public bool Unattackable; // 不可攻击
        public string[] Prerequisites; // 至少需要有哪种单位，才能建造本单位
        public Fix64 Suppliment; // 所占人口
        public int MaxAccessoryNum; // 最大附件数

        public bool IsBuilding; // 是否是建筑
        public bool IsBiological; // 是否是生物单位
        public bool IsMechanical; // 是否是机械单位
        public bool IsAirForce; // 是否是空中单位
        public bool IsAccessory; // 是否是挂件
        public bool NoBody; // 无尸体，死亡立刻移除
        public bool OutOfControl; // 自主行动，不受控制

        public int[] CardsCost; // 各等级升级所需卡片数
    }

    /// <summary>
    /// 单位配置信息管理
    /// </summary>
    public class UnitConfiguration : Component
    {
        static UnitConfiguration Instance
        {
            get { return instance; }
        } static UnitConfiguration instance = null;

        // 所有单位配置信息
        StableDictionary<string, UnitConfigInfo> cfgs = new StableDictionary<string, UnitConfigInfo>();

        public static string[] AllConfigNames
        {
            get { return Instance.cfgs.KeyArray; }
        }

        public UnitConfiguration()
        {
            if (instance != null)
                throw new Exception("only one UnitFactory should be created.");

            instance = this;

            BuildingConfigs();
            BattleUnitConfigs();
            NeutralUnitConfig();
        }

        public static UnitConfigInfo GetDefaultConfig(string type)
        {
            return Instance.cfgs[type];
        }

        public static UnitConfigInfo GetConfig(string type, int lv)
        {
            if (lv <= 1)
                return GetDefaultConfig(type);
            else
            {
                var cfg = GetDefaultConfig(type);
                if (cfg.IsBuilding)
                    cfg.Defence += lv - 1; // 建筑物每升一级防御 + 1
                else
                {
                    // 战斗单位升单双级分别增加攻击和防御
                    if (cfg.Power != 0)
                    {
                        var powerAdd = cfg.Power * (lv / 2) * 0.1;
                        powerAdd = powerAdd < 1 ? 1 : 0;
                        cfg.Power += powerAdd;
                        var defenceAdd = (lv - 1) / 2;
                        cfg.Defence += defenceAdd;
                    }
                    else // 无攻击能力的单位，每级加的都是防御
                    {
                        var defenceAdd = lv - 1;
                        cfg.Defence += defenceAdd;
                    }
                }

                return cfg;
            }
        }

        // 建筑单位
        void BuildingConfigs()
        {
            // 指挥中心
            var cc = new UnitConfigInfo();
            cc.DisplayName = "基地";
            cc.IsBuilding = true;
            cc.MaxHp = 800;
            cc.Cost = 300;
            cc.ConstructingTime = 40;
            cc.Defence = 1;
            cc.GenUnitTypes = new string[] { "CCAcc" };
            cc.GenResourceType = new string[] { "Money", };
            cc.GenResourceRate = new Fix64[] { 6 };
            cc.ShowSize = 40;
            cc.RealSize = 30;
            cc.VisionRadius = 200;
            cc.MaxAccessoryNum = 6;
            cc.LevelUps = new string[] { "StarTrack", "Fortress" };
            cc.CardsCost = new int[] { 2, 5 };
            cfgs["CommanderCenter"] = cc;

            // 星轨
            var st = new UnitConfigInfo();
            st.DisplayName = "星轨";
            st.IsBuilding = true;
            st.MaxHp = 1000;
            st.Cost = 200;
            st.ConstructingTime = 20;
            st.Defence = 1;
            st.GenUnitTypes = new string[] { "CCAcc" };
            st.GenResourceType = new string[] { "Money" };
            st.GenResourceRate = new Fix64[] { 8 };
            st.ShowSize = 40;
            st.RealSize = 30;
            st.VisionRadius = 200;
            st.Prerequisites = new string[] { "Airport" };
            st.MaxAccessoryNum = 6;
            st.CardsCost = new int[] { 2 };
            st.LevelUpFrom = "CommanderCenter";
            cfgs["StarTrack"] = st;

            // 要塞
            var ft = new UnitConfigInfo();
            ft.DisplayName = "要塞";
            ft.IsBuilding = true;
            ft.MaxHp = 1000;
            ft.Cost = 200;
            ft.ConstructingTime = 30;
            ft.Power = 20;
            ft.AttackInterval = 1;
            ft.AttackRange = 200;
            ft.GuardingRange = ft.AttackRange;
            ft.CanAttackGround = true;
            ft.CanAttackAir = false;
            ft.Defence = 3;
            ft.GenUnitTypes = new string[] { "CCAcc" };
            ft.GenResourceType = new string[] { "Money" };
            ft.GenResourceRate = new Fix64[] { 6 };
            ft.ShowSize = 40;
            ft.RealSize = 30;
            ft.VisionRadius = 250;
            ft.Prerequisites = new string[] { "Factory" };
            ft.MaxAccessoryNum = 6;
            ft.CardsCost = new int[] { 2 };
            ft.LevelUpFrom = "CommanderCenter";
            cfgs["Fortress"] = ft;

            // 指挥中心附件
            var ccAcc = new UnitConfigInfo();
            ccAcc.IsBuilding = true;
            ccAcc.IsAccessory = true;
            ccAcc.Unattackable = true; // 挂件一般不承受直接伤害
            ccAcc.DisplayName = "矿机";
            ccAcc.Cost = 50;
            ccAcc.ConstructingTime = 15;
            ccAcc.MaxHp = 50;
            ccAcc.GenResourceType = new string[] { "Money" };
            ccAcc.GenResourceRate = new Fix64[] { 1 };
            ccAcc.ShowSize = 10;
            ccAcc.RealSize = 0;
            ccAcc.Prerequisites = new string[] { "CommanderCenter" };
            ccAcc.CardsCost = new int[] { 2, 5 };
            cfgs["CCAcc"] = ccAcc;

            // 兵营
            var brk = new UnitConfigInfo();
            brk.IsBuilding = true;
            brk.DisplayName = "兵营";
            brk.Cost = 125;
            brk.ConstructingTime = 15;
            brk.MaxHp = 500;
            brk.Defence = 1;
            brk.GenUnitTypes = new string[] { "BrkAcc", "Soldier", "Firebat", "Ghost" };
            brk.ShowSize = 40;
            brk.RealSize = 30;
            brk.VisionRadius = 150;
            brk.Prerequisites = new string[] { "CommanderCenter", "StarTrack", "Fortress" };
            brk.MaxAccessoryNum = 2;
            brk.CardsCost = new int[] { 2, 5 };
            cfgs["Barrack"] = brk;

            // 兵营附件
            var brkAcc = new UnitConfigInfo();
            brkAcc.IsBuilding = true;
            brkAcc.IsAccessory = true;
            brkAcc.Unattackable = true; // 挂件一般不承受直接伤害
            brkAcc.DisplayName = "产能+1";
            brkAcc.Cost = 100;
            brkAcc.ConstructingTime = 25;
            brkAcc.MaxHp = 50;
            brkAcc.GenUnitTypes = new string[] { "Soldier", "Firebat", "Ghost" };
            brkAcc.ShowSize = 10;
            brkAcc.RealSize = 0;
            brkAcc.Prerequisites = new string[] { "Barrack" };
            brkAcc.CardsCost = new int[] { 2, 5 };
            cfgs["BrkAcc"] = brkAcc;

            // 工厂
            var factory = new UnitConfigInfo();
            factory.IsBuilding = true;
            factory.DisplayName = "工厂";
            factory.Cost = 200;
            factory.ConstructingTime = 20;
            factory.MaxHp = 500;
            factory.Defence = 1;
            factory.GenUnitTypes = new string[] { "FctAcc", "Tank", "Robot" };
            factory.ShowSize = 40;
            factory.RealSize = 30;
            factory.VisionRadius = 150;
            factory.MaxAccessoryNum = 1;
            factory.Prerequisites = new string[] { "Barrack" };
            factory.CardsCost = new int[] { 2, 5 };
            cfgs["Factory"] = factory;

            // 工厂附件
            var fctAcc = new UnitConfigInfo();
            fctAcc.IsBuilding = true;
            fctAcc.IsAccessory = true;
            fctAcc.Unattackable = true; // 挂件一般不承受直接伤害
            fctAcc.DisplayName = "产能+1";
            fctAcc.Cost = 100;
            fctAcc.ConstructingTime = 25;
            fctAcc.MaxHp = 50;
            fctAcc.GenUnitTypes = new string[] { "Tank", "Robot" };
            fctAcc.ShowSize = 10;
            fctAcc.RealSize = 0;
            fctAcc.Prerequisites = new string[] { "Factory" };
            fctAcc.CardsCost = new int[] { 2, 5 };
            cfgs["FctAcc"] = fctAcc;

            // 星港
            var airport = new UnitConfigInfo();
            airport.IsBuilding = true;
            airport.DisplayName = "机场";
            airport.Cost = 200;
            airport.ConstructingTime = 20;
            airport.MaxHp = 500;
            airport.Defence = 1;
            airport.GenUnitTypes = new string[] { "ApAcc", "Banshee", "Viking" };
            airport.ShowSize = 40;
            airport.RealSize = 30;
            airport.VisionRadius = 150;
            airport.MaxAccessoryNum = 1;
            airport.Prerequisites = new string[] { "Barrack" };
            airport.CardsCost = new int[] { 2, 5 };
            cfgs["Airport"] = airport;

            // 星港附件
            var apAcc = new UnitConfigInfo();
            apAcc.IsBuilding = true;
            apAcc.IsAccessory = true;
            apAcc.Unattackable = true; // 挂件一般不承受直接伤害
            apAcc.DisplayName = "产能+1";
            apAcc.Cost = 100;
            apAcc.ConstructingTime = 25;
            apAcc.MaxHp = 50;
            apAcc.GenUnitTypes = new string[] { "Banshee", "Viking" };
            apAcc.ShowSize = 10;
            apAcc.RealSize = 0;
            apAcc.Prerequisites = new string[] { "Airport" };
            apAcc.CardsCost = new int[] { 2, 5 };
            cfgs["ApAcc"] = apAcc;

            // 小炮台
            var bunker = new UnitConfigInfo();
            bunker.IsBuilding = true;
            bunker.DisplayName = "小炮台";
            bunker.Cost = 75;
            bunker.ConstructingTime = 10;
            bunker.Power = 5;
            bunker.CanAttackGround = true;
            bunker.CanAttackAir = false;
            bunker.AttackInterval = 0.5f;
            bunker.MaxHp = 75;
            bunker.Defence = 0;
            bunker.ShowSize = 18;
            bunker.RealSize = 15;
            bunker.VisionRadius = 120;
            bunker.AttackRange = 100;
            bunker.GuardingRange = bunker.AttackRange;
            bunker.LevelUps = new string[] { "BigBunker" };
            bunker.Prerequisites = new string[] { "Barrack" };
            bunker.CardsCost = new int[] { 2, 5 };
            cfgs["Bunker"] = bunker;

            // 大炮台
            var bigBunker = new UnitConfigInfo();
            bigBunker.IsBuilding = true;
            bigBunker.DisplayName = "大炮台";
            bigBunker.Cost = 75;
            bigBunker.ConstructingTime = 20;
            bigBunker.Power = 25;
            bigBunker.CanAttackGround = true;
            bigBunker.CanAttackAir = false;
            bigBunker.AttackInterval = 1f;
            bigBunker.MaxHp = 200;
            bigBunker.Defence = 3;
            bigBunker.ShowSize = 25;
            bigBunker.RealSize = 15;
            bigBunker.VisionRadius = 150;
            bigBunker.AttackRange = 120;
            bigBunker.GuardingRange = bigBunker.AttackRange;
            bigBunker.Prerequisites = new string[] { "Factory" };
            bigBunker.CardsCost = new int[] { 2, 5 };
            bigBunker.LevelUpFrom = "Bunker";
            cfgs["BigBunker"] = bigBunker;

            // 雷达标记
            var radarSign = new UnitConfigInfo();
            radarSign.DisplayName = "雷达";
            radarSign.Cost = 50;
            radarSign.MaxHp = 5;
            radarSign.ShowSize = 100;
            radarSign.RealSize = 0;
            radarSign.VisionRadius = 250;
            radarSign.Unattackable = true;
            radarSign.Prerequisites = new string[] { "CommanderCenter", "StarTrack", "Fortress" };
            radarSign.NoBody = true;
            radarSign.CardsCost = new int[] { 2, 5 };
            cfgs["RadarSign"] = radarSign;

            // 治疗塔，初始赠送，不可主动建造
            var healingTower = new UnitConfigInfo();
            healingTower.DisplayName = "治疗塔";
            healingTower.IsBuilding = true;
            healingTower.Cost = 9999999;
            healingTower.MaxHp = 10;
            healingTower.Power = -5;
            healingTower.AOEAreaType = "Pie";
            healingTower.AOEParams = new Fix64[] { 400 /* 治疗范围 */ };
            healingTower.CanAttackAir = true;
            healingTower.CanAttackGround = true;
            healingTower.AttackRange = 0; // 治疗塔并不主动搜索主目标，这个参数没有影响
            healingTower.AttackInterval = 5;
            healingTower.ShowSize = 10;
            healingTower.RealSize = 0;
            healingTower.VisionRadius = 10;
            healingTower.NoBody = true;
            cfgs["HealingTower"] = healingTower;
        }

        // 作战单位
        void BattleUnitConfigs()
        {
            // 枪兵
            var sd = new UnitConfigInfo();
            sd.IsBiological = true;
            sd.DisplayName = "枪兵";
            sd.Cost = 50;
            sd.ConstructingTime = 12;
            sd.Power = 5;
            sd.CanAttackGround = true;
            sd.CanAttackAir = true;
            sd.AttackInterval = 0.5f;
            sd.VisionRadius = 160;
            sd.GuardingRange = 80;
            sd.AttackRange = 60;
            sd.Defence = 0;
            sd.MaxHp = 50;
            sd.MaxVelocity = 40;
            sd.ShowSize = 12;
            sd.RealSize = 2;
            sd.Suppliment = 1;
            sd.Prerequisites = new string[] { "Barrack" };
            sd.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            cfgs["Soldier"] = sd;

            // 火蝠
            var fb = new UnitConfigInfo();
            fb.IsBiological = true;
            fb.DisplayName = "喷火兵";
            fb.Cost = 100;
            fb.ConstructingTime = 15;
            fb.Power = 6;
            fb.CanAttackGround = true;
            fb.CanAttackAir = false;
            fb.AttackInterval = 1.5f;
            fb.VisionRadius = 160;
            fb.GuardingRange = 80;
            fb.AttackRange = 40;
            fb.Defence = 1;
            fb.MaxHp = 100;
            fb.MaxVelocity = 40;
            fb.ShowSize = 15;
            fb.RealSize = 5;
            fb.AOEAreaType = "Fan";
            fb.AOEParams = new Fix64[] { 75, /* 火焰距离 */ 120, /* 角度范围 */ };
            fb.Suppliment = 2;
            fb.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            fb.Prerequisites = new string[] { "Barrack" };
            cfgs["Firebat"] = fb;

            // 鬼兵
            var ghost = new UnitConfigInfo();
            ghost.IsBiological = true;
            ghost.DisplayName = "狙击手";
            ghost.Cost = 100;
            ghost.ConstructingTime = 20;
            ghost.Power = 35;
            ghost.CanAttackGround = true;
            ghost.CanAttackAir = true;
            ghost.AttackInterval = 2f;
            ghost.VisionRadius = 160;
            ghost.GuardingRange = 120;
            ghost.AttackRange = 120;
            ghost.Defence = 0;
            ghost.MaxHp = 60;
            ghost.MaxVelocity = 40;
            ghost.ShowSize = 12;
            ghost.RealSize = 2;
            ghost.Suppliment = 3;
            ghost.CardsCost = new int[] { 2, 5 };
            ghost.Prerequisites = new string[] { "Barrack" };
            cfgs["Ghost"] = ghost;

            // 机器人
            var robot = new UnitConfigInfo();
            robot.DisplayName = "机器人";
            robot.IsMechanical = true;
            robot.Cost = 150;
            robot.ConstructingTime = 20;
            robot.Power = 15;
            robot.CanAttackGround = true;
            robot.CanAttackAir = true;
            robot.AttackInterval = 0.75f;
            robot.VisionRadius = 200;
            robot.GuardingRange = 150;
            robot.AttackRange = 120;
            robot.Defence = 1;
            robot.MaxHp = 125;
            robot.MaxVelocity = 40;
            robot.ShowSize = 15;
            robot.RealSize = 5;
            robot.Suppliment = 3;
            robot.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            robot.Prerequisites = new string[] { "Factory" };
            cfgs["Robot"] = robot;

            // 坦克
            var tank = new UnitConfigInfo();
            tank.DisplayName = "坦克";
            tank.IsMechanical = true;
            tank.Cost = 200;
            tank.ConstructingTime = 25;
            tank.Power = 40;
            tank.CanAttackGround = true;
            tank.CanAttackAir = false;
            tank.AttackInterval = 2;
            tank.VisionRadius = 220;
            tank.GuardingRange = 110;
            tank.AttackRange = 90;
            tank.Defence = 2;
            tank.MaxHp = 150;
            tank.MaxVelocity = 35;
            tank.ShowSize = 20;
            tank.RealSize = 10;
            tank.Suppliment = 4;
            tank.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            tank.Prerequisites = new string[] { "Factory" };
            cfgs["Tank"] = tank;

            // 女妖
            var banshee = new UnitConfigInfo();
            banshee.DisplayName = "女妖";
            banshee.IsMechanical = true;
            banshee.Cost = 200;
            banshee.ConstructingTime = 20;
            banshee.Power = 20;
            banshee.CanAttackGround = true;
            banshee.CanAttackAir = false;
            banshee.AttackInterval = 1;
            banshee.VisionRadius = 200;
            banshee.GuardingRange = 100;
            banshee.AttackRange = 80;
            banshee.Defence = 1;
            banshee.MaxHp = 150;
            banshee.MaxVelocity = 50;
            banshee.ShowSize = 20;
            banshee.RealSize = 10;
            banshee.Suppliment = 3;
            banshee.IsAirForce = true;
            banshee.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            banshee.Prerequisites = new string[] { "Airport" };
            cfgs["Banshee"] = banshee;

            // 维京
            var viking = new UnitConfigInfo();
            viking.DisplayName = "维京";
            viking.IsMechanical = true;
            viking.Cost = 150;
            viking.ConstructingTime = 15;
            viking.Power = 20;
            viking.CanAttackGround = false;
            viking.CanAttackAir = true;
            viking.AttackInterval = 1;
            viking.VisionRadius = 200;
            viking.GuardingRange = 100;
            viking.AttackRange = 80;
            viking.Defence = 1;
            viking.MaxHp = 100;
            viking.MaxVelocity = 50;
            viking.ShowSize = 20;
            viking.RealSize = 10;
            viking.Suppliment = 2;
            viking.IsAirForce = true;
            viking.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            viking.Prerequisites = new string[] { "Airport" };
            cfgs["Viking"] = viking;

            // 伞兵
            var sdCarrier = new UnitConfigInfo();
            sdCarrier.DisplayName = "伞兵";
            sdCarrier.IsMechanical = true;
            sdCarrier.Cost = 150;
            sdCarrier.IsAirForce = true;
            sdCarrier.Power = 3;
            sdCarrier.MaxHp = 50;
            sdCarrier.Defence = 0;
            sdCarrier.RealSize = 0;
            sdCarrier.ShowSize = 35;
            sdCarrier.VisionRadius = 200;
            sdCarrier.MaxVelocity = 200;
            sdCarrier.OutOfControl = true;
            sdCarrier.NoBody = true;
            sdCarrier.CardsCost = new int[] { 2, 5, 10, 20, 50 };
            sdCarrier.Prerequisites = new string[] { "StarTrack" };
            cfgs["SoldierCarrier"] = sdCarrier;
        }

        // 中立单位
        void NeutralUnitConfig()
        {
            // 开局临时视野
            var tmpVisionAtBeginning = new UnitConfigInfo();
            tmpVisionAtBeginning.DisplayName = "开局临时视野";
            tmpVisionAtBeginning.MaxHp = 999999;
            tmpVisionAtBeginning.RealSize = 0;
            tmpVisionAtBeginning.VisionRadius = 10000;
            tmpVisionAtBeginning.Unattackable = true;
            tmpVisionAtBeginning.NoBody = true;
            tmpVisionAtBeginning.NeutralType = true;
            cfgs["TempVisionAtBeginning"] = tmpVisionAtBeginning;

            // 临时视野
            var tmpVision = new UnitConfigInfo();
            tmpVision.DisplayName = "临时视野";
            tmpVision.MaxHp = 5;
            tmpVision.RealSize = 0;
            tmpVision.VisionRadius = 100;
            tmpVision.Unattackable = true;
            tmpVision.NoBody = true;
            tmpVision.NeutralType = true;
            cfgs["TempVision"] = tmpVision;

            // 宝箱
            var treasureBox = new UnitConfigInfo();
            treasureBox.DisplayName = "宝箱";
            treasureBox.MaxHp = 1;
            treasureBox.RealSize = 1;
            treasureBox.ShowSize = 15;
            treasureBox.Unattackable = true;
            treasureBox.NeutralType = true;
            treasureBox.VisionRadius = 40;
            cfgs["TreasureBox"] = treasureBox;

            // 宝箱投放机
            var tbCarrier = new UnitConfigInfo();
            tbCarrier.DisplayName = "宝箱投放机";
            tbCarrier.IsAirForce = true;
            tbCarrier.MaxHp = 1;
            tbCarrier.RealSize = 0;
            tbCarrier.ShowSize = 35;
            tbCarrier.VisionRadius = 120;
            tbCarrier.Unattackable = true;
            tbCarrier.MaxVelocity = 200;
            tbCarrier.NeutralType = true;
            cfgs["TreasureBoxCarrier"] = tbCarrier;

            // 矿点
            var ccStub = new UnitConfigInfo();
            ccStub.DisplayName = "矿点";
            ccStub.GenUnitTypes = new string[] { "CommanderCenter" };
            ccStub.MaxHp = 3000;
            ccStub.RealSize = 0;
            ccStub.ShowSize = 25;
            ccStub.Unattackable = true;
            ccStub.NeutralType = true;
            cfgs["CCStub"] = ccStub;
        }
    }
}
